<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Dollar kurs sozlama';
?>
<div class="square-settings-index">

    <div class="row">
        <div class="col-md-6">
            <h2><?= Html::encode($this->title) ?></h2>

        </div>
        <div class="col-md-6">
            <p>
            <h2>
                <?= Html::a('Yangi kurs qoshish', ['index.php/settings/dollar-create'], ['class' => 'btn btn-success pull-right']) ?>
            </h2>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'format' => 'raw',
                        'label' => 'Narx',
                        'value' => function ($data) {
                            if (isset($data->price) && !empty($data->price)) {
                                return $data->price;
                            }
                            return '';
                        }
                    ],
                    [
                        'format' => 'raw',
                        'label' => 'Qoshilgan sana',
                        'value' => function ($data) {
                            if (isset($data->start_date) && !empty($data->start_date)) {
                                return date('d.m.Y H:i', strtotime($data->start_date));
                            }
                            return '';
                        }
                    ],
                    [
                        'format' => 'raw',
                        'label' => 'Tugalanish sana',
                        'value' => function ($data) {
                            if (isset($data->start_date) && !empty($data->start_date)) {
                                if (!(isset($data->end_date) && !empty($data->end_date))) {
                                    return '<span class="label label-success">Faol</span>';
                                }
                                return date('d.m.Y H:i', strtotime($data->start_date));
                            }
                            return '';
                        }
                    ],
                    [
                        'format' => 'raw',
                        'value' => function ($data, $id) use ($model) {
                            if (!isset($data->end_date) || empty($data->end_date)) {
                                if (count($model) > 1) {
                                    return '<a href="delete-dollar?id='.$id.'" onclick="return confirm(\'Siz ushbu dollar kursni ochirish istaysizmi?\')"><i class="fa fa-trash" aria-hidden="true" ></i></a>';
                                }
                            }
                            return '';
                        }
                    ]

                ],
            ]); ?>
        </div>
    </div>



</div>