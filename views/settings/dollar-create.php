<?php

use yii\helpers\Html;


$this->title = 'Yangi kurs qoshish';
?>
<div class="dollar-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('dollar-_form', [
        'model' => $model,
    ]) ?>

</div>
