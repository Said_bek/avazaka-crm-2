<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Foydalanovchilar modullari';
?>
<div class="square-settings-index">

    <div class="row">
        <div class="col-md-12">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class='table table'>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'format' => 'raw',
                            'label' => 'Foydalanovchi',
                            'value' => function ($data) {
                                $name = '';
                                if (isset($data->second_name) && !empty($data->second_name)) {
                                    $name .= $data->second_name;
                                }
                                return $name;
                            }
                        ],
                        [
                            'format' => 'raw',
                            'label' => 'Modular',
                            'contentOptions' => ['style' => 'width:50%;'],
                            'value' => function ($data) use ($users_module) {
                                $active_module = '';
                                if (isset($users_module[$data->user_id]) && !empty($users_module[$data->user_id])) {
                                    foreach ($users_module[$data->user_id] as $user_module) {
                                        if ($user_module['status'] == 1) {
                                            $active_module .= '<span class="label label-default" >' . $user_module['module_name'] . '</span> ';
                                        }
                                    }
                                }
                                if ($active_module == '') {
                                    $active_module = '<i>Bu foyfalanovchi hech qanday modulga biriktiralmagan</i>';
                                }
                                return $active_module;
                            }
                        ],


                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return '<a href="module-view?id=' . $model->user_id . '" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a> ';
                                },
                                'update' => function ($url, $model) {
                                    return '<a href="module-update?id=' . $model->user_id . '" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>';
                                },
                                'delete' => function ($url, $model) {
                                    // return '<a href="module-delete?id=' . $model->user_id . '" title="Delete" aria-label="Delete" data-pjax="0" data-confirm="Siz ushbu ele" data-method="post"><span class="glyphicon glyphicon-trash"></span></a>';
                                    return '';
                                },
                            ]
                        ],
                    ],
                ]); ?>
            </table>
        </div>
    </div>





</div>