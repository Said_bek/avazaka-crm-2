<?php

use yii\helpers\Html;


$this->title = 'Kvadrat-metr qoshish';
?>
<div class="square-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
