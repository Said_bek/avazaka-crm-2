<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="dollar-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'price')->textInput(['type' => 'number','step'=>'any','min'=> '0.1'])->label('Narx') ?>


    <div class="form-group">
        <?= Html::submitButton('Salash', ['class' => 'btn btn-success','id' => 'stop-class']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

$js = <<<JS
        
    $(document).on("click","#stop-class",function(){
        var check_input = $('#coursesettings-price').val();
        var _this  = $(this)
        if (typeof(check_input) != "undefined" && check_input !== null) {
            if (parseInt(check_input) > 0) {
                setTimeout(function(){
                    $(_this).attr('disabled',true);
                    $(_this).attr('readonly',true);
                }, 1);
            }
        }
    });
        

JS;


$this->registerJs($js);
?>