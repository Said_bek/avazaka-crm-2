<?php

use yii\helpers\Html;
use kartik\select2\Select2;

$this->title = $user->second_name.' ning modul ozgartirish';
?>

<?= Html::beginForm(['index.php/settings/module-update','id' => $_GET['id']], 'post', ['enctype' => 'multipart/form-data']) ?>

<div class="row">
    <div class="col-md-12">
        <h2><?= Html::encode($this->title)?></h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= '<label class="control-label">Modullar</label>' ?>
        <?= Select2::widget([
            'name' => 'module',
            'id' => 'user-module',
            'data' => $modules,
            'value' => $user_modules,
            'options' => [
                'multiple' => true,
                'allowClear' => true
            ],
        ]); ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success pull-right']) ?>
    </div>
</div>
<?= Html::endForm() ?>