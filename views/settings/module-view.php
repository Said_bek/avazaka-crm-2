<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $user->second_name . ' ning Modullari';

?>
<div class="moduel-settings-view">
    <div class="row">
        <div class="col-md-6">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-md-6">
            <h1>
                <p>
                    <?= Html::a('Ozgartirish', ['/index.php/settings/module-update', 'id' => $user->user_id], ['class' => 'btn btn-primary pull-right']) ?>
                </p>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= DetailView::widget([
                'model' => $user_modules,
                'attributes' => [
                    [
                        'label' => 'Buyurtmalar',
                        'format' => 'html',
                        'value' => function ($data) {
                            $status = '<span class="label label-danger">O\'chiq</span>';
                            if ($data) {
                                foreach ($data as $user_module) {
                                    if ($user_module['module_id'] == 1 && $user_module['status'] == 1) {
                                        $status = '<span class="label label-success">Aktiv</span>';
                                    }
                                }
                            }
                            return $status;
                        }
                    ],
                    [
                        'label' => 'Tayyor mahsulotlar ombori',
                        'format' => 'html',
                        'value' => function ($data) {
                            $status = '<span class="label label-danger">O\'chiq</span>';
                            if ($data) {
                                foreach ($data as $user_module) {
                                    if ($user_module['module_id'] == 2 && $user_module['status'] == 1) {
                                        $status = '<span class="label label-success">Aktiv</span>';
                                    }
                                }
                            }
                            return $status;
                        }
                    ],
                    [
                        'label' => 'Foydalanuvchilar',
                        'format' => 'html',
                        'value' => function ($data) {
                            $status = '<span class="label label-danger">O\'chiq</span>';
                            if ($data) {
                                foreach ($data as $user_module) {
                                    if ($user_module['module_id'] == 3 && $user_module['status'] == 1) {
                                        $status = '<span class="label label-success">Aktiv</span>';
                                    }
                                }
                            }
                            return $status;
                        }
                    ],
                    [
                        'label' => 'Seh bo’limlari',
                        'format' => 'html',
                        'value' => function ($data) {
                            $status = '<span class="label label-danger">O\'chiq</span>';
                            if ($data) {
                                foreach ($data as $user_module) {
                                    if ($user_module['module_id'] == 4 && $user_module['status'] == 1) {
                                        $status = '<span class="label label-success">Aktiv</span>';
                                    }
                                }
                            }
                            return $status;
                        }
                    ],
                    [
                        'label' => 'Mijozlar',
                        'format' => 'html',
                        'value' => function ($data) {
                            $status = '<span class="label label-danger">O\'chiq</span>';
                            if ($data) {
                                foreach ($data as $user_module) {
                                    if ($user_module['module_id'] == 5 && $user_module['status'] == 1) {
                                        $status = '<span class="label label-success">Aktiv</span>';
                                    }
                                }
                            }
                            return $status;
                        }
                    ],
                    [
                        'label' => 'Hisobotlar',
                        'format' => 'html',
                        'value' => function ($data) {
                            $status = '<span class="label label-danger">O\'chiq</span>';
                            if ($data) {
                                foreach ($data as $user_module) {
                                    if ($user_module['module_id'] == 6 && $user_module['status'] == 1) {
                                        $status = '<span class="label label-success">Aktiv</span>';
                                    }
                                }
                            }
                            return $status;
                        }
                    ],
                    [
                        'label' => 'Moliya bo’limi',
                        'format' => 'html',
                        'value' => function ($data) {
                            $status = '<span class="label label-danger">O\'chiq</span>';
                            if ($data) {
                                foreach ($data as $user_module) {
                                    if ($user_module['module_id'] == 7 && $user_module['status'] == 1) {
                                        $status = '<span class="label label-success">Aktiv</span>';
                                    }
                                }
                            }
                            return $status;
                        }
                    ],
                    [
                        'label' => 'Sozlamalar',
                        'format' => 'html',
                        'value' => function ($data) {
                            $status = '<span class="label label-danger">O\'chiq</span>';
                            if ($data) {
                                foreach ($data as $user_module) {
                                    if ($user_module['module_id'] == 8 && $user_module['status'] == 1) {
                                        $status = '<span class="label label-success">Aktiv</span>';
                                    }
                                }
                            }
                            return $status;
                        }
                    ],
                    [
                        'label' => 'So’rovnomalar yuborish',
                        'format' => 'html',
                        'value' => function ($data) {
                            $status = '<span class="label label-danger">O\'chiq</span>';
                            if ($data) {
                                foreach ($data as $user_module) {
                                    if ($user_module['module_id'] == 9 && $user_module['status'] == 1) {
                                        $status = '<span class="label label-success">Aktiv</span>';
                                    }
                                }
                            }
                            return $status;
                        }
                    ],
                    [
                        'label' => 'Xizmat turlari',
                        'format' => 'html',
                        'value' => function ($data) {
                            $status = '<span class="label label-danger">O\'chiq</span>';
                            if ($data) {
                                foreach ($data as $user_module) {
                                    if ($user_module['module_id'] == 10 && $user_module['status'] == 1) {
                                        $status = '<span class="label label-success">Aktiv</span>';
                                    }
                                }
                            }
                            return $status;
                        }
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>