<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Branch;
use yii\helpers\ArrayHelper;
use app\models\BranchCategories;
use app\models\CategoryGlass;
use app\models\Glass;

$branchs = Branch::find()->all();
$glasses = Glass::find()->all();


?>

<div class="category-form">
    <?php if (Yii::$app->session->hasFlash('danger')) : ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i>Xatolik!</h4>
            <?= Yii::$app->session->getFlash('danger') ?>
        </div>
    <?php endif; ?>
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Kategorya nomi') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label for="">Filiallar</label>
            <select name="Category[branchs][]" multiple="multiple" id="branch" class="form-control">
                <?php foreach ($branchs as $key => $value) : $selected = ''; ?>
                    <?php if (isset($model->id)) : ?>

                        <?php $find = BranchCategories::find()->where(['category_id' => $model->id, 'branch_id' => $value->id])->one(); ?>

                        <?php if (isset($find)) : ?>
                            <?php $selected = 'selected'; ?>
                        <?php endif ?>

                    <?php endif ?>
                    <option <?php echo $selected; ?> value="<?php echo $value->id ?>">
                        <?php echo $value->title ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>

    <div class="row" style="margin-top: 13px;">
        <div class="col-md-12">
            <label for="">Oyna birktirish</label>
            <select name="Category[glass][]" multiple="multiple" id="glass" class="form-control">
                <?php foreach ($glasses as $glass) : $selected = '';
                    $category_glass = CategoryGlass::find()->where(['=', 'category_id', $model->id])->andWhere(['=', 'glass_id', $glass->id])->one()
                ?>
                    <?php if (!empty($category_glass->id)) : ?>
                        <?php $selected = 'selected'; ?>
                    <?php endif ?>
                    <option <?php echo $selected; ?> value="<?php echo $glass->id ?>">
                        <?php echo $glass->title ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>


    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success pull-right']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs(
    '$(function(){
        $("#branch").select2();
        $("#glass").select2();
    })',
    yii\web\View::POS_READY
); ?>