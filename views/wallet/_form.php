<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$model->price_type = 1;
?>

<div class="wallet-pay-form">

    <div class="row">
        <?php $form = ActiveForm::begin(['id' => 'wallet_form']); ?>
        <div class="col-md-12">
            <!-- Notification block -->
            <div class="row">
                <div class="col-md-12" id='notificatin_block'>
                </div>
            </div>

            <!-- Mijoz tanlash select2 -->
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'client_id')->widget(Select2::class, [
                        'data' => $clients,
                        'pluginOptions' => [
                            'style' => 'width:100%',
                            'allowClear' => false,
                        ]
                    ])->label('Foydalanovchi') ?>
                </div>
            </div>

            <!-- Pull joyi input varadio -->
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'price')->textInput(['type' => 'number', 'step' => 'any'])->label('Narx') ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'price_type')->radioList([1 => 'som', 2 => 'dollar'])->label('Narx turi') ?>
                </div>
            </div>

            <!-- Toldirish knopka -->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton('Toldirish', ['class' => 'btn btn-success pull-right', 'id' => 'save-stop']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


</div>
<?php

$js = <<<JS
    window.onload = function() {
        $().alert()
    }

    $(document).on("click","#save-stop",function(event){
        event.preventDefault()
        var _this = $(this)
        var client = $('#walletpay-client_id option:selected').text()
        var price = $('#walletpay-price').val()
        var price_type = $('#walletpay-price_type').children().children('input:checked').val()
        var price_type_text = ''
        var form_serialize =  $('#wallet_form').serialize()
        if (price_type == 1) {
            price_type_text = 'som'
        }else if(price_type == 2){
            price_type_text = 'dollar'
        }
        var message = 'Siz '+client+' ga '+price+' '+price_type_text+' toldirish istaysizmi '

        setTimeout(() => {
            if (confirm(message)) {
                $(_this).attr('disabled',true)
                $(_this).attr('readonly',true)
                $.ajax({
                    type: "POST",
                    url: "pay",
                    data: form_serialize,
                    dataType: "JSON",
                    success: function (response) {
                        $(_this).attr('disabled',false)
                        $(_this).attr('readonly',false)
                        let not_bl  = $('#notificatin_block')
                        if (response.status == 'success') {
                            response.message.forEach(function (message, i){
                                var text = `<div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>`+message+`
                                            </div>`
                                not_bl.append(text)
                            });
                        }else if(response.status == 'error'){
                            response.message.forEach(function (message, i){
                                if (i == 0) {
                                    var text = `<div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>`+message+`
                                    </div>`    
                                }else{
                                    var text = `<div class="alert alert-warning alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>`+message+`
                                    </div>`
                                }
                                not_bl.append(text)
                            });
                        }else {
                            var text = `<div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Pull toldirshda xato ketdi 
                            </div>`
                            not_bl.append(text)
                        }
                        $().alert()
                    }
                });
            }
        }, 1);
    })

JS;


$this->registerJs($js);
?>