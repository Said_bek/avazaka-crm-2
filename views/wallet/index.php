<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Pull toldirish';
?>
<div class="wallet-pay-create">

    <div class="row">
        <div class="col-md-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $this->render('_form', [
                'model' => $model,
                'clients' => $clients,
            ]) ?>
        </div>
    </div>


</div>