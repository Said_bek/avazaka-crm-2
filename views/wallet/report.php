<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

if (isset($clients) && !empty($clients)) {
    $time_arr = [];
    foreach ($clients as $id => $full_name) {
        $time_arr[] = $id;
    }
    $model->client_id = $time_arr;
}
$this->title = 'Hisobot';
$model->price_type = [1, 2];
?>

<!-- Form -->
<div class="row">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin(['id' => 'wallet_form']); ?>
        <!-- Filte and title -->
        <div class="row">
            <div class="col-md-10">
                <h1>Hisobot</h1>
            </div>
            <div class="col-md-1">
                <h2>
                    <div class="form-group">
                        <?= Html::button('Excel', ['class' => 'btn btn-info pull-right', 'id' => 'excel']) ?>
                    </div>
                </h2>

            </div>
            <div class="col-md-1">
                <h2>
                    <div class="form-group">
                        <?= Html::button('Filtrlash', ['class' => 'btn btn-success pull-right', 'id' => 'filter']) ?>
                    </div>
                </h2>
            </div>
        </div>
        <!-- Filter block -->
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'client_id')->widget(Select2::class, [
                    'data' => $clients,
                    'pluginOptions' => [
                        'style' => 'width:100%',
                        'allowClear' => false,
                        'multiple' => true
                    ]
                ])->label('Foydalanovchilar') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'created_date')->widget(DatePicker::class, [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy'
                    ]
                ])->label('Qoshilgan sana') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'price_type')->checkboxList([1 => 'som', 2 => 'dollar'])->label('Narx turi') ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<!-- Table -->
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Mijoz</th>
                    <th>Narx</th>
                    <th>Qoshilgan sana</th>
                </tr>
            </thead>
            <tbody id='report-tbody'>
                <?php
                if (isset($wallet_pays) && !empty($wallet_pays)) {
                    $i = 0;
                    foreach ($wallet_pays as $wallet_pay) {
                        $check_id = $wallet_pay->client_id;
                        $wallet_pay->client_id = '';
                        foreach ($clients as $id => $full_name) {
                            if ($check_id == $id) {
                                $wallet_pay->client_id = $full_name;
                                break;
                            }
                        }

                        $price_text = '';
                        if (strrpos($wallet_pay->price, '.')) {
                            $price_text = number_format($wallet_pay->price, strlen($wallet_pay->price) - strrpos($wallet_pay->price, '.') - 1, ',', ' ');
                        } else {
                            $price_text = number_format($wallet_pay->price, 0, ',', ' ');
                        }

                        if ($wallet_pay->price_type == 1) {
                            $price_text .= ' som';
                        } elseif ($wallet_pay->price_type == 2) {
                            $price_text .= ' dollar';
                        }

                ?>
                        <tr>
                            <td><?= ++$i ?></td>
                            <td><?= $wallet_pay->client_id ?></td>
                            <td><?= $price_text ?></td>
                            <td><?= date('d.m.Y H:i', strtotime($wallet_pay->created_date)) ?></td>
                        </tr>
                    <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="4">Malumot yoq</td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<?php

$js = <<<JS

$(document).on("click","#stop-class",function(){
    var _this = $(this)
})

JS;


$this->registerJs($js);
?>