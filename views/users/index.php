<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Foydalanuvchilar';
$this->params['breadcrumbs'][] = $this->title;
?>


<style>
    #w0 .summary {
        display: none;
    }
</style>
<div class="users-index">
    <?php if (Yii::$app->session->hasFlash('danger')) : ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i>Xatolik!</h4>
            <?= Yii::$app->session->getFlash('danger') ?>
        </div>
    <?php endif; ?>
    <?php if (Yii::$app->session->hasFlash('success')) : ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i>Muvaffaqiyatli!</h4>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="col-md-6">
            <h2>
                <?= Html::a('Foydalanuvchi qo`shish', ['index.php/users/create'], ['class' => 'btn btn-success pull-right']) ?>
            </h2>
        </div>
    </div>
    <hr>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'second_name',
            'phone_number',
            [
                'format' => 'html',
                'label' => 'Ro`li',
                'value' => function ($data) {
                    $role = '';
                    if (isset($data->type)) {
                        if ($data->type == 0) {
                            $role = "Admin";
                        } elseif ($data->type == 1) {
                            $role = "Seh boshlig’i";
                        } elseif ($data->type == 2) {
                            $role = "Sotuv menejeri";
                        } elseif ($data->type == 3) {
                            $role = "Brigadir";
                        } elseif ($data->type == 4) {
                            $role = "Menejer";
                        } elseif ($data->type == 5) {
                            $role = "Buxgalter";
                        }
                    }
                    return $role;
                },
            ],
            [
                'format' => 'html',
                'label' => 'Filiallari',
                'value' => function ($data) {
                    $span = '';
                    foreach ($data->branchs as $key => $value) {
                        $span .= '<span class="label label-default">' . $value->branch->title . '</span> ';
                    }

                    return $span;
                },
            ],


            [
                'class' => 'yii\grid\ActionColumn',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to(['/index.php/users/' . $action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>

<?php

$js = <<<JS
    $(function(){
        $('.table').dataTable({
            "paging": false
        } );

        let sss = $(".sss").width()
        $(".ddd").width(sss)

        let vvv = $(".vvv").width()
        $(".fixed").width(vvv)

       
        
    })

    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        if(scroll >= 220){
            $('.fixed').css('top',0)
        }
        else{
            $('.fixed').css('top','-100%')
        }
    });

JS;


$this->registerJs($js);
?>