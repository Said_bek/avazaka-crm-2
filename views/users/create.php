<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Foydalanuvchi qo`shish';
?>
<div class="users-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'branchs' => $branchs,
        'user_branchs' => $user_branchs,
        'roles' => $roles,
    ]) ?>

</div>
