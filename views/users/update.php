<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'O`zgartirish: ' . $model->name;
?>
<div class="users-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'branchs' => $branchs,
        'user_branchs' => $user_branchs,
        'roles' => $roles,
    ]) ?>

</div>
