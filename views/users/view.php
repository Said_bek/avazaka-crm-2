<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->name;
\yii\web\YiiAsset::register($this);
?>
<div class="users-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('O`zgartirish', ['/index.php/users/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Ochirish', ['/index.php/users/delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Foydalanuvchini o`chirishni istaysizmi?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'second_name',
            'phone_number',
            [
                'format' => 'html',
                'label' => 'Ro`li',
                'value' => function ($data) {
                    $role = '';
                    if (isset($data->type)) {
                        if ($data->type == 0) {
                            $role = "Admin";
                        } elseif ($data->type == 1) {
                            $role = "Seh boshlig’i";
                        } elseif ($data->type == 2) {
                            $role = "Sotuv menejeri";
                        } elseif ($data->type == 3) {
                            $role = "Brigadir";
                        } elseif ($data->type == 4) {
                            $role = "Menejer";
                        } elseif ($data->type == 5) {
                            $role = "Buxgalter";
                        }
                    }
                    return $role;
                },
            ],
            [
                'format' => 'html',
                'label' => 'Filiallari',
                'value' => function ($data) {
                    $span = '';
                    foreach ($data->branchs as $key => $value) {
                        $span .= '<span class="label label-default">' . $value->branch->title . '</span> ';
                    }

                    return $span;
                },
            ],
        ],
    ]) ?>

</div>