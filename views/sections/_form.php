<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sections */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sections-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Nomi') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($section_time, 'work_time')->textInput(['maxlength' => true, 'type' => 'number'])->label('Ishlash soati') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success pull-right save-stop']) ?>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
<?php

$js = <<<JS

$(document).on('click','.save-stop',function(){
    var _this = $(this)
    var title = $('#sections-title')
    var work_time = $('#sectiontimes-work_time')
    setTimeout(function(){

        if ($(title).val().trim() != '') {
            if ($(work_time).val().trim() != '') {
                $(_this).attr('disabled',true)
                $(_this).attr('readonly',true)
            }    
        }
    }, 1);
})

JS;
$this->registerJs($js);
?>