<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Detalar';
?>
<div class="category-index">
    <div class="row">
        <div class="col-md-6">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="col-md-6">
            <h2>
                <?= Html::a('Detal qo`shish', ['index.php/details/create'], ['class' => 'btn btn-success pull-right']) ?>
            </h2>
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'format' => 'html',
                'label' => 'Nomi',
                'value' => function ($data) {
                    return $data->title;
                },
            ],
            [
                'format' => 'html',
                'label' => 'Yaratilgan sana',
                'value' => function ($data) {
                    return date('d.m.Y H:i', strtotime($data->created_date));
                },
            ],
            [
                'format' => 'html',
                'label' => 'Holati',
                'value' => function ($data) {
                    return (($data->status == 1) ? '<span class="label label-success">Faol</span>' : '<span class="label label-danger">Arxiv</span>');
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to(['index.php/details/' . $action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>