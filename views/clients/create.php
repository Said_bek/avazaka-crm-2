<?php

use yii\helpers\Html;

$this->title = 'Mijoz qo`shish';
?>
<div class="clients-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'branchs' => $branchs,
    ]) ?>

</div>
