<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Branch;
use kartik\date\DatePicker;
use kartik\select2\Select2;
$data_update = 0;
$data_id = 0;
if (isset($model->id) && !empty($model->id)) {
    $data_update = 1;
    $data_id = $model->id;
}
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'full_name')->textInput(['maxlength' => true])->label('Ism Familiya') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true, 'pattern' => '\\+?\d+','data-update' => $data_update,'data-id' => $data_id])->label('Telefon raqam') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'branch_id')->widget(Select2::classname(), [
                'data' => $branchs,
                'options' => ['placeholder' => 'Filial tanlang'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Filial') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'brith_date')->widget(DatePicker::class, [
                'options' => ['placeholder' => 'Mijozni tugilgan sana tanlang'],
                'pluginOptions' => [
                    'format' => 'dd.mm.yyyy',
                    'autoclose' => true
                ]
            ])->label('Tug\'ilgan sana') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'type')->radioList([2 => 'B to C', 1 => 'B to B'], ['value' => 2])->label('Mijoz turi') ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'company')->textInput(['maxlength' => true])->label('Firma nomi') ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'inn')->textInput(['maxlength' => true])->label('INN') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success pull-right check_number']) ?>
            </div>
        </div>
    </div>




    <?php ActiveForm::end(); ?>

</div>
<?php
$js = <<<JS
	
    $(document).on("click",".check_number",function(event){
        var _this = $(this)
        event.preventDefault()
        // event.stopPropagation()
        var phone_number = $('#clients-phone_number').val()
        if (
            $('#clients-full_name').val() != '' && 
            $('#clients-phone_number').val() != '' && 
            $('#clients-branch_id').val() != '' && 
            $('#clients-brith_date').val() != '' 
        ) {
            $.ajax({
                url: 'chekc-number',
                dataType: 'json',
                type: 'POST',
                data:{
                    phone_number: phone_number,
                    data_update: $('#clients-phone_number').attr('data-update'),
                    data_id: $('#clients-phone_number').attr('data-id')
                },
                success: function (response) {
                    if (response.status == 'success') {
                        $(_this).submit()
                        $(_this).attr('disabled' ,true)
                        $(_this).attr('readonly' ,true)
                    } else if (response.status == 'fail') {
                        $('#clients-phone_number').parent().removeClass('has-success')
                        $('#clients-phone_number').parent().addClass('has-error')
                        $('#clients-phone_number').siblings('.help-block').text('Bunday Telefon raqam mavjud')
                    } else {
                        $('#clients-phone_number').parent().removeClass('has-success')
                        $('#clients-phone_number').parent().addClass('has-error')
                        $('#clients-phone_number').siblings('.help-block').text('Telfon raqam yoq yoki notigri')
                    }
                }
            });
        }
    });

JS;


$this->registerJs($js);
?>