<?php

use yii\helpers\Html;

$this->title = 'O`zgartirish: ' . $model->full_name;
?>
<div class="clients-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'branchs' => $branchs,
    ]) ?>

</div>
