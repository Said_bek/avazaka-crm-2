<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = base64_decode($model->full_name);
\yii\web\YiiAsset::register($this);
?>
<div class="clients-view">

    <h1><?=  Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('O`zgartirish', ['/index.php/clients/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('O`chirish', ['/index.php/clients/delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Mijozni o`chirmoqchimisiz?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'format' => 'html',
                'label' => 'Familiya Ismi',
                'value' => function ($data) {
                    return base64_decode($data->full_name);
               },
            ],
            [
                'format' => 'html',
                'label' => 'Telefon raqami',
                'value' => function ($data) {
                    return $data->phone_number;
               },
            ],
            [
                'format' => 'html',
                'label' => 'Holati',
                'value' => function ($data) {
                    return (($data->status == 1) ? '<span class="label label-success">Faol</span>' : '<span class="label label-danger">Arxiv</span>');
               },
            ],
            [
                'format' => 'html',
                'label' => 'Filial',
                'value' => function ($data) {
                    if (isset($data->branch->title)) {
                        return $data->branch->title;
                    }else {
                        return '';
                    }
               },
            ],
            [
                'format' => 'html',
                'label' => 'Mijoz turi',
                'value' => function ($data) {
                    if (isset($data->type)) {
                        if ($data->type == 1) {
                            return 'B to B';
                        }else {
                            return 'B to C';
                        }
                    }else {
                        return '';
                    }
               },
            ],
            [
                'format' => 'html',
                'label' => 'Tug\'ilgan sana',
                'value' => function ($data) {
                    if (isset($data->brith_date)) {
                        return date('d.m.Y',strtotime($data->brith_date));
                    }else {
                        return '';
                    }
               },
            ],
            [
                'format' => 'html',
                'label' => 'Firma',
                'value' => function ($data) {
                    if (isset($data->company)) {
                        return base64_decode($data->company);
                    }else {
                        return '';
                    }
               },
            ],
            [
                'format' => 'html',
                'label' => 'INN',
                'value' => function ($data) {
                    if (isset($data->inn)) {
                        return $data->inn;
                    }else {
                        return '';
                    }
               },
            ],
        ],
    ]) ?>

</div>
