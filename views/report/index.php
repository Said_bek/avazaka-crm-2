<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

function diff_date($date1, $date2)
{

	if ($date2 != NULL) {
		$t1 = strtotime($date1); //kottasi
		$t2 = strtotime($date2);
		$diff = $t2 - $t1;
		$hours = $diff / (60 * 60);

		return round($hours);
	} else {
		$t1 = strtotime($date1); //kottasi
		$t2 = strtotime(date('Y-m-d H:i:s'));
		$diff = $t2 - $t1;
		$hours = $diff / (60 * 60);

		return round($hours);
	}
}

function begin_tag($tag)
{
	echo '<' . $tag . '>';
}
function end_tag($tag)
{
	echo '</' . $tag . '>';
}
function tag_text($text)
{
	echo $text;
}
function date_formating($date)
{
	return date('d.m.Y H:i', strtotime($date));
}

?>

<style>
	/* .table>tbody>tr>td,
	.table>tfoot>tr>td {
		padding: 4px;
	} */

	.fixed {
		/*width: 95.23%;*/
		position: fixed;
		top: -100%;
		transition: .5s;
	}

	.overflow {
		overflow-x: auto;
		/*position: relative !important;*/
	}
</style>

<div class="row">
	<div class="col-md-12">
		<h2>Bitgan buyurtmalar bo'yicha hisobot</h2>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php $form = ActiveForm::begin(); ?>

		<div class="row">
			<div class="col-md-2">
				<?= $form->field($filter_model, 'id')->textInput(['type' => 'number'])->label('Kodi') ?>
			</div>
			<div class="col-md-4">
				<?php
				if (!$filter_have) {
					$time_arr = [];
					foreach ($clients as $id => $full_name) {
						$time_arr[] = $id;
					}
					$filter_model->client_id = $time_arr;
				} else {
					if (isset($filter['client_id']) && !empty($filter['client_id'])) {
						$time_arr = [];
						foreach ($filter['client_id'] as $id) {
							$time_arr[] = $id;
						}
						$filter_model->client_id = $time_arr;
					}
				}
				?>
				<?= $form->field($filter_model, 'client_id')->widget(Select2::class, [
					'data' => $clients,
					'pluginOptions' => [
						'multiple' => true,
						'style' => ['width:100%;']
					],
				])->label('Mijozlar') ?>
			</div>
			<div class="col-md-2">
				<?= $form->field($filter_model, 'created_date')->widget(DatePicker::class, [
					'pluginOptions' => [
						'autoclose' => true,
						'format' => 'dd.mm.yyyy'
					]
				])->label('Buyurtma olingan sana') ?>
			</div>
			<div class="col-md-2">
				<?= $form->field($filter_model, 'end_date')->widget(DatePicker::class, [
					'pluginOptions' => [
						'autoclose' => true,
						'format' => 'dd.mm.yyyy'
					]
				])->label('Bitgan sana') ?>

			</div>
			<div class="col-md-2">
				<?php
				$checked_dollar  = '';
				$checked_som  = '';
				if (!$filter_have) {
					$checked_dollar  = 'checked';
					$checked_som  = 'checked';
				} else {
					if (isset($filter_model->price_type['som']) && !empty($filter_model->price_type['som'])) {
						$checked_som  = 'checked';
					}
					if (isset($filter_model->price_type['dollar']) && !empty($filter_model->price_type['dollar'])) {
						$checked_dollar  = 'checked';
					}
				}
				?>
				<label for="order_title">Narx turi</label>
				<div class="row">
					<div class="col-md-2">
						<label for="price_type_som">Som</label>
					</div>
					<div class="col-md-2">
						<input type="checkbox" name='Orders[price_type][som]' <?= $checked_som ?> id="price_type_som" value='1'>
					</div>
					<div class="col-md-3">
						<label for="price_type_dollar">Dollar</label>
					</div>
					<div class="col-md-2">
						<input type="checkbox" name='Orders[price_type][dollar]' <?= $checked_dollar ?> id="price_type_dollar" value='2'>
					</div>
					<div class="col-md-3"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10"></div>
			<div class="col-md-2">
				<div class="form-group">
					<?= Html::submitButton('Filtrlash', ['class' => 'btn btn-success pull-right']) ?>
				</div>
			</div>
		</div>


		<?php ActiveForm::end(); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="overflow">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Kodi</th>
						<th>Nomi</th>
						<th>Narxi</th>
						<th>Mijoz</th>
						<th>Buyurtma olingan sana</th>
						<th>Ishlab chiqarishga berilgan sana</th>
						<th>Rejadagi bitish sanasi</th>
						<th>Bitgan sana</th>
						<th>Ishlab chiqarishga ketgan vaqt</th>
					</tr>
				</thead>
				<tbody id='report-tbody'>

					<?php
					if (isset($orders) && !empty($orders)) {
						$i = 0;
						foreach ($orders as $order) {
							begin_tag('tr');

							begin_tag('td');
							tag_text(++$i);
							end_tag('td');

							begin_tag('td');
							tag_text('#' . $order->id);
							end_tag('td');


							begin_tag('td');
							tag_text($order->title);
							end_tag('td');

							begin_tag('td');
							
							if (strrpos($order->price, '.')) {
								tag_text(number_format($order->price, strlen($order->price) - strrpos($order->price, '.') - 1, ',', ' '));
							} else {
								tag_text(number_format($order->price, 0, ',', ' '));
							}
							if ($order->price_type == 1) {
								tag_text(' som');
							} else {
								tag_text(' $');
							}
							
							end_tag('td');


							begin_tag('td');
							if (isset($order->client->full_name) && !empty($order->client->full_name)) {
								tag_text(base64_decode($order->client->full_name));
							}
							end_tag('td');

							begin_tag('td');
							tag_text(date_formating($order->created_date));
							end_tag('td');

							begin_tag('td');
							if (isset($exit_dates) && !empty($exit_dates)) {
								foreach ($exit_dates as $exit_date) {
									if ($exit_date->order_id == $order->id) {
										tag_text(date_formating($exit_date->exit_date));
										break;
									}
								}
							}
							end_tag('td');

							begin_tag('td');
							tag_text(date_formating($order->dead_line));
							end_tag('td');

							begin_tag('td');
							tag_text(date_formating($order->end_date));
							end_tag('td');

							$label = diff_date($order->created_date, $order->end_date);
							if ($label > 0) {
								$label  = '<span class="label label-danger">'.$label.'</span>';
							}elseif($label < 0) {
								$label  = '<span class="label label-success">'.$label.'</span>';
							}

							begin_tag('td');
							tag_text($label);
							end_tag('td');


							end_tag('tr');
						}
					} else {
					?>
						<tr>
							<td colspan="10">Malumotlar yoq</td>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<?php

$js = <<<JS
    $(function(){
        $(".sidebar-toggle").trigger("click");

        let sss = $(".sss").width()
        $(".ddd").width(sss)

        let vvv = $(".vvv").width()
        $(".fixed").width(vvv)

        let a = $('.pagination li').length
        for(let i = 1; i <= a; i++){
        	let d = $('.pagination li').eq(i).find('a').attr('href')
        	$('.pagination li').eq(i).find('a').attr('href','/index.php'+d)
        }
        let dd = $('.pagination .prev').find('a').attr('href')
        $('.pagination .prev').find('a').attr('href','/index.php'+dd)

        
    })

    $(window).scroll(function (event) {
	    var scroll = $(window).scrollTop();
	    if(scroll >= 220){
	    	$('.fixed').css('top',0)
	    }
	    else{
	    	$('.fixed').css('top','-100%')
	    }
	});

JS;


$this->registerJs($js);
?>