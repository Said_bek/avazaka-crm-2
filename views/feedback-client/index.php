<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Klient savollari';
?>
<div class="feedback-client-index">

    <div class="row">
        <div class="col-md-6">
            <h2><?= Html::encode($this->title) ?></h2>        
        </div>
        <div class="col-md-6">
            <h2>
                <?= Html::a("Savol qo'shish", ['/index.php/feedback-client/create'], ['class' => 'btn btn-success pull-right']) ?>
            </h2>        
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'format' => 'html',
                'label' => 'Savol uz',
                'value' => function ($data) {
                    return base64_decode($data->title_uz);
               },
            ],
            [
                'format' => 'html',
                'label' => 'Savol ru',
                'value' => function ($data) {
                    return base64_decode($data->title_ru);
               },
            ],
            [
                'format' => 'html',
                'label' => 'Ball sistema',
                'value' => function ($data) {
                    if ($data->ball_type == 1) {
                        return '5 ballik';
                    } else if($data->ball_type == 2) {
                        return '10 ballik';
                    }else {
                        return '';
                    }
               },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to(['index.php/feedback-client/'.$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>
