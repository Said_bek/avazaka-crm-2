<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FeedbackClient */

$model->title_uz = base64_decode($model->title_uz);
$model->title_ru = base64_decode($model->title_ru);
$this->title = 'Savolni o`zgartirish: ' . $model->title_uz;
?>
<div class="feedback-client-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>