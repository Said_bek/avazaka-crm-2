<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\FeedbackClient */

$this->title = base64_decode($model->title_uz);
\yii\web\YiiAsset::register($this);
?>
<div class="feedback-client-view">

    <div class="row">
        <div class="col-md-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>
                <?= Html::a('O`zgartirish', ['/index.php/feedback-client/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('O`chirish', ['/index.php/feedback-client/delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => "Savolni o'chirmoqchimisiz?",
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'format' => 'html',
                        'label' => 'Savol uz',
                        'value' => function ($data) {
                            return base64_decode($data->title_uz);
                        },
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Savol ru',
                        'value' => function ($data) {
                            return base64_decode($data->title_ru);
                        },
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Ball sistema',
                        'value' => function ($data) {
                            if ($data->ball_type == 1) {
                                return '5 ballik';
                            } else if ($data->ball_type == 2) {
                                return '10 ballik';
                            }
                        },
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Qoshilgan sana',
                        'value' => function ($data) {
                            return date('d.m.Y H:i', strtotime($data->created_date));
                        },
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'format' => 'html',
                        'label' => 'Mijoz',
                        'value' => function ($data) use ($clients) {
                            if (isset($clients, $data->client_id) && !empty($clients) && !empty($data->client_id)) {
                                foreach ($clients as $client) {
                                    if ($client->id == $data->client_id) {
                                        return base64_decode($client->full_name);
                                        break;
                                    }
                                }
                            }
                            return '';
                        },
                    ],

                    [
                        'format' => 'html',
                        'label' => 'Ball',
                        'value' => function ($data) use ($model) {
                            if (isset($data->ball, $model->ball_type) && !empty($data->ball) && !empty($model->ball_type)) {
                                if ($model->ball_type == 1) {
                                    return $data->ball . ' / 5';
                                } else {
                                    return $data->ball . ' / 10';
                                }
                            }
                            return '';
                        },
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Tavsif',
                        'value' => function ($data) {
                            if (isset($data->description) && !empty($data->description)) {
                                return base64_decode($data->description);
                            }
                            return '';
                        },
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Jonatilgan sana',
                        'value' => function ($data) {
                            if (isset($data->created_date) && !empty($data->created_date)) {
                                return date('d.m.Y H:i', strtotime($data->created_date));
                            }
                            return '';
                        },
                    ],
                ],
            ]) ?>
        </div>
    </div>


</div>