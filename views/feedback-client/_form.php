<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeedbackClient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-client-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title_uz')->textarea(['rows' => 4])->label('Savol uz') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'title_ru')->textarea(['rows' => 4])->label('Savol ru') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'ball_type')->radioList(['1' => '5 ball', '2' => '10 ball'])->label('Ballik sistema') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success pull-right']) ?>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>