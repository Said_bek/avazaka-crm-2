<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = 'Klient javoblari';
?>
<div class="feedback-client-balls">

    <div class="row">
        <div class="col-md-12">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'format' => 'html',
                        'label' => 'Mijoz',
                        'value' => function ($data) use ($clients) {
                            if (isset($clients, $data->client_id) && !empty($clients) && !empty($data->client_id)) {
                                foreach ($clients as $client) {
                                    if ($client->id == $data->client_id) {
                                        return base64_decode($client->full_name);
                                        break;
                                    }
                                }
                            }
                            return '';
                        },
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Savol uz',
                        'value' => function ($data) use ($questions) {
                            if (isset($questions, $data->feedback_client_id) && !empty($questions) && !empty($data->feedback_client_id)) {
                                foreach ($questions as $question) {
                                    if ($question->id == $data->feedback_client_id) {
                                        return base64_decode($question->title_uz);
                                        break;
                                    }
                                }
                            }
                            return '';
                        },
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Savol ru',
                        'value' => function ($data) use ($questions) {
                            if (isset($questions, $data->feedback_client_id) && !empty($questions) && !empty($data->feedback_client_id)) {
                                foreach ($questions as $question) {
                                    if ($question->id == $data->feedback_client_id) {
                                        return base64_decode($question->title_ru);
                                        break;
                                    }
                                }
                            }
                            return '';
                        },
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Ball',
                        'value' => function ($data) use ($questions) {
                            if (isset($data->ball, $questions) && !empty($data->ball) && !empty($questions)) {
                                foreach ($questions as $question) {
                                    if ($question->id == $data->feedback_client_id) {
                                        if ($question->ball_type == 1) {
                                            return $data->ball.' / 5';
                                        }else {
                                            return $data->ball.' / 10';
                                        }
                                        break;
                                    }
                                }
                            }
                            return '';
                        },
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Tavsif',
                        'value' => function ($data) {
                            if (isset($data->description) && !empty($data->description)) {
                                return base64_decode($data->description);
                            }
                            return '';
                        },
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Jonatilgan sana',
                        'value' => function ($data) {
                            if (isset($data->created_date) && !empty($data->created_date)) {
                                return date('d.m.Y H:i',strtotime($data->created_date));
                            }
                            return '';
                        },
                    ],
                ],
            ]) ?>

        </div>
    </div>
</div>