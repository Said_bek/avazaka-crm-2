<?php

use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

?>
<form id="form">

	<div class="row row-padd">
		<div class="col-md-6">
			<label for="order_title">Buyurtma nomi</label>
			<input name="title" type="text" required class="form-control" id="order_title">
		</div>
		<div class="col-md-6">
			<label for="branch">Filial</label>
			<?= Select2::widget([
				'name' => 'branch',
				'data' => $branchs,
				'size' => Select2::MEDIUM,
				'options' => [
					'required' => true,
					'id' => 'branch',
					'width' => '100%',
					'class' => 'form-control',
					'placeholder' => 'Filial tanlang'
				],
				'pluginOptions' => [],
			]) ?>
			</select>
		</div>
	</div>

	<div class="row row-padd">
		<div class="col-md-4">
			<label for="client">Mijozlar</label>
			<?= Select2::widget([
				'name' => 'client',
				'data' => $clients,
				'size' => Select2::MEDIUM,
				'options' => [
					'required' => true,
					'id' => 'client',
					'placeholder' => 'Mijoz tanlang'
				],
				'pluginOptions' => [],
			]) ?>
		</div>
		<div class="col-md-4">
			<label for="category">Kategoriyalar</label>
			<?= Select2::widget([
				'name' => 'category',
				'data' => $categorys,
				'size' => Select2::MEDIUM,
				'options' => [
					'required' => true,
					'id' => 'category',
					'placeholder' => 'Kategoriya tanlang'
				],
				'pluginOptions' => [],
			]) ?>
		</div>
		<div class="col-md-4">
			<input type="hidden" name="confirm_end_date" id='confirm_end_date' value="">
			<label for="end_date">Buyurtma bitish sanasi</label>
			<?= DateTimePicker::widget([
				'name' => 'end_date',
				'id' => 'end_date',
				'options' => ['placeholder' => 'Bitish sanansi'],
				'pluginOptions' => [
					'autoclose' => true,
					'format' => 'dd-mm-yyyy hh:ii',
					'autocomplete' => 'off',
				]
			]) ?>
		</div>
	</div>

	<div class="row row-padd">
		<div class="col-md-4">
			<label for="order_square">Kvadrat-metr</label>
			<input name="square" type="number" required class="form-control" id="order_square">
		</div>
		<div class="col-md-4">
			<label for="order_price">Narxi</label>
			<input name="price" type="number" required class="form-control" id="order_price">
		</div>
		<div class="col-md-4">
			<label for="order_title">Narx turi</label>
			<div class="row">
				<div class="col-md-2">
					<label for="price_type_som">Som</label>
				</div>
				<div class="col-md-2">
					<input type="radio" name='price_type' id="price_type_som" value='1' checked>
				</div>
				<div class="col-md-2">
					<label for="price_type_dollar">Dollar</label>
				</div>
				<div class="col-md-2">
					<input type="radio" name='price_type' id="price_type_dollar" value='2'>
				</div>
			</div>
		</div>

	</div>

	<div class="row row-padd">
		<div class="col-md-12">
			<label for="description">Qo'shimcha izoh</label>
			<textarea name="description" id='description' class="form-control"></textarea>
		</div>
	</div>

	<div class="row row-padd">
		<div class="col-md-6">
			<label>Bolimlar tanlash</label>
		</div>
		<div class="col-md-6">
			<label>Buyurtma detalari</label>
		</div>
	</div>

	<div class="row row-padd">
		<div class="col-md-6">
			<div class="row" id='sections-collector' data-counter='0'></div>
			<div class="row">
				<div class="col-md-12">
					<span class="btn btn-success pull-right btn-flat" id='add-sections'><i class="fa fa-plus" aria-hidden="true"></i></span>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row" id='details-collector' data-counter='0'></div>
			<div class="row">
				<div class="col-md-12">
					<span class="btn btn-success pull-right btn-flat" id='add-details'><i class="fa fa-plus" aria-hidden="true"></i></span>
				</div>
			</div>
		</div>
	</div>

</form>

<?php $this->registerJs(
	'
    $("#end_date").attr("autocomplete", "off");
',
	yii\web\View::POS_READY
); ?>