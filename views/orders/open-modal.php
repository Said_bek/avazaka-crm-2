<?php

function diff_date($date1, $date2)
{

    $date1 = strtotime($date1);
    $date2 = strtotime($date2);

    // Formulate the Difference between two dates 
    $diff = abs($date2 - $date1);


    // To get the year divide the resultant date into 
    // total seconds in a year (365*60*60*24) 
    $years = floor($diff / (365 * 60 * 60 * 24));


    // To get the month, subtract it with years and 
    // divide the resultant date into 
    // total seconds in a month (30*60*60*24) 
    $months = floor(($diff - $years * 365 * 60 * 60 * 24)
        / (30 * 60 * 60 * 24));


    // To get the day, subtract it with years and  
    // months and divide the resultant date into 
    // total seconds in a days (60*60*24) 
    $days = floor(($diff - $years * 365 * 60 * 60 * 24 -
        $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));


    // To get the hour, subtract it with years,  
    // months & seconds and divide the resultant 
    // date into total seconds in a hours (60*60) 
    $hours = floor(($diff - $years * 365 * 60 * 60 * 24
        - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24)
        / (60 * 60));


    // To get the minutes, subtract it with years, 
    // months, seconds and hours and divide the  
    // resultant date into total seconds i.e. 60 
    $minutes = floor(($diff - $years * 365 * 60 * 60 * 24
        - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24
        - $hours * 60 * 60) / 60);


    // To get the minutes, subtract it with years, 
    // months, seconds, hours and minutes  
    $seconds = floor(($diff - $years * 365 * 60 * 60 * 24
        - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24
        - $hours * 60 * 60 - $minutes * 60));

    return printf("%d kun, %d soat, "
        . "%d minut", $days, $hours, $minutes);
}

?>
<h4 class="btn btn-danger delete_order" order-id="<?php echo $order->id ?>">Buyurtmani o'chirish</h4>

<?php if (!$order_control) : ?>
    <h4 class="btn btn-primary back_order" order-id="<?php echo $order->id ?>">Buyurtmani ortga qaytarish</h4>
<?php endif ?>
<hr>
<table class="table table-bordered table-striped">
    <tr>
        <th>Buyurtma kodi</th>
        <td>
            <?php echo $order->id ?>
        </td>
    </tr>
    <tr>
        <th>Buyurtma nomi</th>
        <td>
            <?php echo $order->title ?>
        </td>
    </tr>
    <tr>
        <th>Buyurtma holati</th>
        <td>
            <?php if ($order->pause == 0) : ?>
                <div class="label label-success">
                    FAOL
                </div>
            <?php else : ?>
                <div class="label label-warning">
                    PAUZA
                </div>
            <?php endif ?>
        </td>
    </tr>
    <tr>
        <th>Mijoz Ismi</th>
        <td>
            <?php echo base64_decode($order->client->full_name) ?>
        </td>
    </tr>
    <tr>
        <th>Mijhoz turi</th>
        <td>
            <?php
            if ($order->client->type == 1) {
                echo 'B to B';
            } else {
                echo 'B to C';
            }
            ?>
        </td>
    </tr>
    <tr>
        <th>Buyurtma narxi</th>
        <td><?php echo $order->price; ?></td>
    </tr>
    <tr>
        <th>Buyurtma pull turi</th>
        <td><?php
            if ($order->price_type == 1) {
                echo 'Som';
            } else {
                echo 'Dollar';
            }
            ?></td>
    </tr>
    <tr>
        <th>Kvadrat-metr</th>
        <td><?php echo $order->square; ?></td>
    </tr>
    <tr>
        <th>Kategoriya nomi</th>
        <td>
            <?php
            echo  $order->category->title;
            ?>
        </td>
    </tr>
    <tr>
        <th>Buyurtma detalari</th>
        <td>
            <div class="row">
                <?php
                if (isset($order_details) && !empty($order_details)) {
                    foreach ($order_details as $detail_title => $detail_count) {
                ?>
                        <div class="col-md-12">
                            <b><?=$detail_title?>:</b>
                            <span><?=$detail_count?> ta</span>
                        </div>
                <?php
                    }
                }
                ?>
            </div>
        </td>
    </tr>
    <tr>
        <th>Buyurtma qoshilgan sanasi</th>
        <td>
            <?php echo date("d.m.Y H:i", strtotime(date($order->created_date))) ?>
        </td>
    </tr>
    <tr>
        <th>Buyurtma bitish sanasi</th>
        <td>
            <?php echo date("d.m.Y H:i", strtotime(date($order->dead_line))) ?>
        </td>
    </tr>
    <tr>
        <th>Qo'shimcha izoh</th>
        <td>
            <textarea name="desc" rows="5" id="desc" class="form-control" style="resize: vertical;"><?php echo $order->description; ?></textarea>
            <br>
            <button class="btn btn-sm btn-success btn-block" id="save_desc" data-id="<?php echo $order->id; ?>">
                Izohni o'zgartirish
            </button>
        </td>
    </tr>
</table>

<hr>
<?php if (!empty($order->order_step)) : ?>

    <?php if (!$order_control) : ?>
        <h4>Buyurtma holati
            <?php if (Yii::$app->user->can('Admin')) { ?>
                <?php if ($pause) : ?>
                    <span class="btn btn-success pull-right" order-id="<?php echo $order->id ?>" pause-id="<?php echo $pause->id ?>" id="active">Faollashtirish</span>
                <?php else : ?>
                    <span class="btn btn-warning pull-right" order-id="<?php echo $order->id ?>" id="pause">STOP</span>
                <?php endif ?>
            <?php } ?>
        </h4>
    <?php endif ?>


    <br>
    <br>
    <br>
    <div class="timeline timeline--label-checkpoint-above">
        <?php foreach ($order->order_step as $key => $value) : ?>

            <div class="timeline-step <?php echo (($value->status == 0) ? 'timeline-step--current' : ''); ?>">
                <div class="timeline-step__checkpoint">
                    <div class="timeline-step__label">
                        <?php echo $value->section->title ?>
                    </div>

                </div>
            </div>

        <?php endforeach ?>
    </div>
    <br>
    <table class="table table-bordered table-striped">
        <tr>
            <th>#</th>
            <th>Bo'lim nomi</th>
            <th>Kirish vaqti</th>
            <th>Chiqish vaqti</th>
            <th>Ketgan vaqt</th>
        </tr>
        <?php $i = 1;
        foreach ($order->order_step as $key => $value) : ?>
            <tr>
                <td><?php echo $i++ ?></td>
                <td>
                    <?php echo $value->section->title ?>
                </td>
                <td>
                    <?php echo $value->section_orders_enter_date_by_step_id ?>
                </td>
                <td>
                    <?php echo $value->section_orders_exit_date ?>
                </td>
                <td>
                    <?php if (isset($value->section_orders_enter_date_by_step_id) && isset($value->section_orders_exit_date) && $value->section_orders_exit_date != NULL) : ?>

                        <?php diff_date($value->section_orders_enter_date_by_step_id, $value->section_orders_exit_date); ?>
                    <?php endif ?>

                </td>
            </tr>
        <?php endforeach ?>
    </table>
<?php endif ?>