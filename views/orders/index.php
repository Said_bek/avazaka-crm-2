<style>
    .row-padd {
        padding-top: 10px;
    }
</style>
<section class="lists-container">
    <div class="list">
        <h3 class="list-title">Buyurtmalar</h3>
        <ul class="list-items">
            <?php if (!empty($static_sections_control)) : ?>
                <?php foreach ($static_sections_control as $static_con) :
                    $style = 'style="border:blue solid 2px"';
                    if ($static_con->order->pause == 1) {
                        $style = 'style="border:orange solid 2px; opacity:.5"';
                    } else if ($static_con->order->section_orders($static_con->order->id, 0) != NULL) {
                        $style = 'style="border:green solid 2px;"';
                    }
                ?>
                    <li <?php echo $style ?> class="open_modal" order-id="<?php echo $static_con->order_id ?>" section-id="<?php echo 0 ?>" data-toggle="modal" data-target="#modal-default2">
                        <b title="Buyurtma nomi">
                            <i class="text-info fa fa-reorder"></i>
                            <?php echo (($static_con->order) ? $static_con->order->title : '') ?>
                        </b>
                        <hr class="m-1">
                        <p title="Mijoz Ismi">
                            <i class="text-info fa fa-user"></i>
                            <?php echo (($static_con->order) ? base64_decode($static_con->order->client->full_name) : '') ?>
                        </p>


                        <?php if (!empty($static_con->order->categories)) : ?>
                            <?php foreach ($static_con->order->categories as $valuev) : ?>
                                <p title="Kategoriya nomi">
                                    <i class="text-info fa fa-bookmark-o"></i>
                                    <?php echo $valuev->category->title ?>
                                </p>

                            <?php endforeach ?>
                        <?php endif; ?>

                        <p title="Filial nomi">
                            <i class="text-info fa fa-bank"></i>
                            <?php echo (($static_con->order) ? $static_con->order->branch->title : '') ?>
                        </p>
                        <hr class="m-1">
                        <b title="Buyurtma bitish sanasi">
                            <i class="text-info fa fa-hourglass-3"></i>
                            <?php echo (($static_con->order) ? date("d.m.Y H:i", strtotime(date($static_con->order->dead_line))) : '')

                            ?>
                        </b>


                    </li>
                <?php endforeach ?>
            <?php endif ?>
        </ul>
        <button class="add-card-btn btn2 add_order" data-toggle="modal" data-target="#modal-default3">
            <i class="fa fa-plus"></i> Buyurtma qo`shish
        </button>
    </div>

    <?php if (!empty($sections)) : ?>
        <?php
        foreach ($sections as $value) : ?>
            <div class="list">
                <h3 class="list-title">
                    <?php echo $value->title ?>
                    <br>
                    <br>
                    <span class="font-lighter text-muted">
                        <?php echo ((count($value->orders) > 0) ?  count($value->orders) . "ta
                         buyurtma" : "Buyurtma yo`q") ?>
                    </span>
                </h3>

                <ul class="list-items">
                    <?php if (!empty($value->orderscontrol)) :
                    ?>
                        <?php foreach ($value->orderscontrol as $valueo) :
                            $style = 'style="border:blue solid 2px"';
                            if ($valueo->order->pause == 1) {
                                $style = 'style="border:orange solid 2px; opacity:.5"';
                            } else if ($valueo->order->section_orders($valueo->order->id, $value->id) != NULL) {
                                $style = 'style="border:green solid 2px;"';
                            }
                        ?>
                            <li <?php echo $style ?> class="open_modal" order-id="<?php echo $valueo->order_id ?>" section-id="<?php echo $value->id ?>" data-toggle="modal" data-target="#modal-default2">
                                <b title="Buyurtma nomi">
                                    <i class="text-info fa fa-reorder"></i>
                                    <?php echo (($valueo->order) ? $valueo->order->title : '') ?>
                                </b>
                                <hr class="m-1">
                                <p title="Mijoz Ismi">
                                    <i class="text-info fa fa-user"></i>
                                    <?php echo (($valueo->order) ? base64_decode($valueo->order->client->full_name) : '') ?>
                                </p>


                                <?php if (!empty($valueo->order->categories)) : ?>
                                    <?php foreach ($valueo->order->categories as $valuev) : ?>
                                        <p title="Kategoriya nomi">
                                            <i class="text-info fa fa-bookmark-o"></i>
                                            <?php echo $valuev->category->title ?>
                                        </p>

                                    <?php endforeach ?>
                                <?php endif; ?>

                                <p title="Filial nomi">
                                    <i class="text-info fa fa-bank"></i>
                                    <?php echo (($valueo->order) ? $valueo->order->branch->title : '') ?>
                                </p>
                                <hr class="m-1">
                                <b title="Buyurtma bitish sanasi">
                                    <i class="text-info fa fa-hourglass-3"></i>
                                    <?php echo (($valueo->order) ? date("d-m-Y H:i", strtotime(date($valueo->order->dead_line))) : '')

                                    ?>
                                </b>
                                <hr class="m-1">
                                <?php if ($valueo->order->parralel == 1) : ?>
                                    <span class="label label-warning">
                                        Shponli buyurtma
                                    </span>
                                <?php endif ?>


                            </li>
                        <?php endforeach ?>
                    <?php endif ?>
                    <?php if (!empty($value->orders)) : ?>
                        <?php foreach ($value->orders as  $valueo) :
                            $style = 'style="border:#3c8dbc solid 2px;background:#3c8dbc52"';
                            if (isset($valueo->order) && $valueo->order->pause == 1) {
                                $style = 'style="border:orange solid 2px; opacity:.5"';
                            } else if (isset($valueo->order) && $valueo->order->section_orders($valueo->order->id, $value->id) != NULL) {
                                $style = 'style="border:green solid 2px;background:#00a65a52"';
                            } else if (isset($valueo->order) && $valueo->order->dead_line < date('Y-m-d H:i:s')) {
                                $style = 'style="border:#dd4b39 solid 2px; background: #dd4b3929"';
                            }
                        ?>
                            <li <?php echo $style ?> class="open_modal" order-id="<?php echo $valueo->order_id ?>" section-id="<?php echo $value->id ?>" data-toggle="modal" data-target="#modal-default2">
                                <b title="Buyurtma nomi">
                                    <i class="text-info fa fa-reorder"></i>
                                    <?php echo (($valueo->order) ? $valueo->order->title : '') ?>
                                </b>
                                <hr class="m-1">
                                <p title="Mijoz Ismi">
                                    <i class="text-info fa fa-user"></i>
                                    <?php echo (($valueo->order) ? base64_decode($valueo->order->client->full_name) : '') ?>
                                </p>


                                <?php if (!empty($valueo->order->categories)) : ?>
                                    <?php foreach ($valueo->order->categories as  $valuev) : ?>
                                        <p title="Kategoriya nomi">
                                            <i class="text-info fa fa-bookmark-o"></i>
                                            <?php echo $valuev->category->title ?>
                                        </p>

                                    <?php endforeach ?>
                                <?php endif; ?>

                                <p title="Filial nomi">
                                    <i class="text-info fa fa-bank"></i>
                                    <?php echo (($valueo->order) ? $valueo->order->branch->title : '') ?>
                                </p>
                                <hr class="m-1">
                                <b title="Buyurtma bitish sanasi">
                                    <i class="text-info fa fa-hourglass-3"></i>
                                    <?php echo (($valueo->order) ? date("d-m-Y H:i", strtotime(date($valueo->order->dead_line))) : '')

                                    ?>
                                </b>
                                <hr class="m-1">
                                <?php if (isset($valueo->order) && $valueo->order->parralel == 1) : ?>
                                    <span class="label label-warning">
                                        Shponli buyurtma
                                    </span>
                                <?php endif ?>
                            </li>
                        <?php endforeach ?>
                    <?php endif ?>

                </ul>
            </div>
        <?php endforeach ?>
    <?php endif ?>
</section>



<?php


$js = <<<JS
    // $(function(){
    //     $(".sidebar-toggle").trigger("click");
    // })
    $(document).on('click','#add-sections',function() {
        var _this = $(this);

        $(_this).attr('disabled',true)
        $(_this).attr('readonly',true)

        var count = parseInt($('#sections-collector').attr('data-counter'));
        var sections_collector = $('#sections-collector');
        $(sections_collector).attr('data-counter',count+1)


        $.ajax({
            url: 'get-all-sections',
            dataType: 'json',
            type: 'GET',
            data: {
                count: count
            },
            success: function (response) {
                if (response.status == 'success') {
                    $(sections_collector).append(response.content);
                    $('#w0').removeAttr('id')
                }else{
                    $(sections_collector).attr('data-counter',0);
                    $(sections_collector).text();
                }
            }
        });

        $(_this).attr('disabled',false)
        $(_this).attr('readonly',false)
        
    });

    $(document).on('click','.delete-this-row',function() {  
        var _this = $(this)
        $(_this).attr('disabled',true)
        $(_this).attr('readonly',true)
        var parent = $(_this).parent().parent().parent()
        $(parent).remove()
    })

    $(document).on('click','#add-details',function() {
        var _this = $(this);

        $(_this).attr('disabled',true)
        $(_this).attr('readonly',true)

        var count = parseInt($('#details-collector').attr('data-counter'));
        var details_collector = $('#details-collector');
        $(details_collector).attr('data-counter',count+1)


        $.ajax({
            url: 'get-all-details',
            dataType: 'json',
            type: 'GET',
            data: {
                count: count
            },
            success: function (response) {
                if (response.status == 'success') {
                    $(details_collector).append(response.content);
                    $('#w0').removeAttr('id')
                }else{
                    $(details_collector).attr('data-counter',0);
                    $(details_collector).text();
                }
            }
        });

        $(_this).attr('disabled',false)
        $(_this).attr('readonly',false)
        
    });


    // open_modal
    $(document).on("click",".open_modal",function(){
        $('#modal-default2 .modal-body').html(`<div class="overlay">
              <i class="fa fa-refresh fa-spin"></i>
            </div>`)
        let order_id = $(this).attr("order-id")
        let section_id = $(this).attr("section-id")

        $.ajax({
            url: '/index.php/orders/open-modal',
            dataType: 'json',
            type: 'GET',
            data:{
                order_id: order_id,
                section_id: section_id,
            },
            success: function (response) {
                if (response.status == 'success') {
                    $('#modal-default2 .modal-body').html(response.content)
                    $('#modal-default2 .modal-header').html(response.header)
                    
                    if(!response.order_control){
                        $('#modal-default2 .modal-footer .btn-primary').addClass('btn-danger')
                        $('#modal-default2 .modal-footer .btn-danger').removeClass('btn-primary')
                        $('#modal-default2 .modal-footer .btn-danger').addClass('order_end')
                        $('.order_end').removeClass('save_order')
                        $('.order_end').removeClass('order_start')
                        $('.order_end').attr('order-id',order_id)
                        $('.order_end').attr('section-id',section_id)
                        $('.order_end').text('Buyurtma bitdi')
                    }
                    else{
                        $('#modal-default2 .modal-footer .btn-danger').addClass('btn-primary')
                        $('#modal-default2 .modal-footer .btn-primary').removeClass('btn-danger')
                        $('#modal-default2 .modal-footer .btn-primary').addClass('order_start')
                        $('.order_start').removeClass('save_order')
                        $('.order_start').removeClass('order_end')
                        $('.order_start').attr('order-id',order_id)
                        $('.order_start').attr('section-id',section_id)
                        $('.order_start').text('Buyurtmani boshlash')
                        $('.order_start').attr('data-id',response.order_control)
                    }

                }
            }
        });

    });

    $(document).on("click",".order_start",function(){
        
        if(confirm('Buyurtmaga start berilishini tasdiqlaysizmi ?')){
            var _this = $(this)
            $(_this).prop('disabled',true);
            var order_id = $(this).attr("order-id");
            var id = $(this).attr('data-id')

            $.ajax({
                url: '/index.php/orders/start-order',
                dataType: 'json',
                type: 'GET',
                data: {order_id: order_id,id: id},
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.reload()
                    }
                    if(response.status == 'failure'){
                        $(_this).prop('disabled',false);
                        alert('Ma`lumot saqlash xatolik!')
                    }   
                }
            });
        }
        else{
            $(_this).prop('disabled',false);
        }
        

    })



    // order_end
    $(document).on("click",".order_end",function(){
        var _this = $(this)
        $(_this).prop('disabled',true)
        let order_id = $(this).attr("order-id")
        let section_id = $(this).attr("section-id")


        if(confirm('Ushbu buyurtmani tugatishni istaysizmi ?')){
            $.ajax({
                url: '/index.php/orders/order-end',
                dataType: 'json',
                type: 'GET',
                data:{
                    order_id: order_id,
                    section_id: section_id,
                },
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.reload()
                    }
                    else{
                        alert('Ma`lumot saqlashda xatolik');
                        $(_this).prop('disabled',false)
                    }
                }
            });
        }
        else{
            $(_this).prop('disabled',false)
        }
    });

    // add_order
    $(document).on("click",".add_order",function(){
        let section_id = $(this).attr("section-id")

        $.ajax({
            url: '/index.php/orders/add-order-modal',
            dataType: 'json',
            type: 'GET',
            success: function (response) {
                if (response.status == 'success') {
                    $('#modal-default3 .modal-body').html(response.content)
                    $('#modal-default3 .modal-header').html(response.header)
                    $('#modal-default3 .modal-footer .btn-success').addClass('save_order')
                    $('.save_order').removeClass('order_end')
                    $('.save_order').text('Saqlash')
                    
                }
            }
        });

    });



    $(document).on("click","#plus",function(){
        let i = parseInt($(this).attr('data-count'));
        let section_id = $(this).attr('section-id');
        $("#plus").attr('data-count',i+1)
        $.ajax({
            url: '/index.php/orders/add-section',
            dataType: 'json',
            type: 'GET',
            data: {i: i,section_id: section_id},
            success: function (response) {
                if (response.status == 'success') {
                    $("#sections_order").append(response.content)
                    
                }   
            }
        });

    })

    $(document).on("click","#minus",function(){
        let leng = $("#sections_order .row").length
        let i = parseInt($('#plus').attr('data-count'));
        if(leng > 1){
            $("#sections_order .row").last().remove() 

        }

        if(i > 2){
            $("#plus").attr('data-count',i-1)
        }
        
        
    })

    // save_order
    $(document).on("click",".save_order",function(){
        const _this = $(this)
        _this.attr("disabled",true)
        _this.attr("readonly",true)

        $.ajax({
            url: '/index.php/orders/save-order',
            dataType: 'json',
            type: 'POST',
            data: $("#form").serialize(),
            success: function (response) {
                console.log(response);
                if (response.status == 'success') {
                    window.location.reload()
                }else if(response.status == 'notification'){
                    $('#confirm_end_date').val($('#end_date').val())
                    alert('Ushbu kunda limit mumkun bolagan soni '+response.limit+' osha kunda boladigan kvadrat-metr '+response.all_limit)
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else if(response.status == 'failure_empty'){
                    alert('Buyurtma bo`limlari yoki ish vaqtlari bo`sh bo`lishi mumkin emas!')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else if(response.status == 'failure_title'){
                    alert('Buyurtma nomi bo`sh bo`lishi mumkin emas!')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else if(response.status == 'failure_branch'){
                    alert('Buyurtma filiali bo`sh bo`lishi mumkin emas!')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else if(response.status == 'failure_client'){
                    alert('Buyurtma mijozi bo`sh bo`lishi mumkin emas!')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else if(response.status == 'failure_category'){
                    alert('Buyurtma kategoriya bo`sh bo`lishi mumkin emas!')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else if(response.status == 'failure_end_date'){
                    alert('Buyurtma tugash sana bo`sh bo`lishi mumkin emas!')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else if(response.status == 'failure_square'){
                    alert('Buyurtma kvadrat-metr bo`sh bo`lishi mumkin emas!')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else if(response.status == 'failure_price'){
                    alert('Buyurtma narxi bo`sh bo`lishi mumkin emas!')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else if(response.status == 'failure_price_type'){
                    alert('Buyurtma bolim bo`lishi shart!')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else if(response.status == 'failure_details'){
                    alert('Buyurtma detalri bo`lishi shart!')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }else{
                    alert('Buyurtma qoshishda xato bor qayta urnib korin yoki brauzer yangilab ishlatin')
                    _this.attr("disabled",false)
                    _this.attr("readonly",false)
                }
                    
            }
        });
    })


    // pause
    $(document).on("click","#pause",function(){
        if(confirm("Buyurtmani vaqtinchalik to'xtatmoqchimisiz ?")){
            let id = $(this).attr('order-id')

            $.ajax({
                url: '/index.php/orders/pause',
                dataType: 'json',
                type: 'GET',
                data: {id: id},
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.reload()
                    }   
                }
            });
        }
    })
    
    // events
    $(document).ready(function(){ 
        $(".sidebarr").fadeIn("slow");
    });

    // active
    $(document).on("click","#active",function(){

        if(confirm("Buyurtmani faollashtirmoqchimisiz ?")){
            let id = $(this).attr('order-id')
            let pause_id = $(this).attr('pause-id')

            $.ajax({
                url: '/index.php/orders/active',
                dataType: 'json',
                type: 'GET',
                data: {id: id,pause_id: pause_id},
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.reload()
                    }   
                }
            });
        }
        

    })


    $(document).on('change','#section',function(){
        let id = $(this).val();
        var _this = $(this)

        $.ajax({
            url: '/index.php/orders/find-time',
            dataType: 'json',
            type: 'GET',
            data: {id: id},
            success: function (response) {
                if (response.status == 'success') {
                    $(_this).parent().parent().find('.time').val(response.content)
                }   
            }
        });

    })


    // delete_order
    $(document).on('click','.delete_order',function(){

        if(confirm('Buyurtmani o`chirishga ishonchingiz komilmi ?')){
            let id = $(this).attr('order-id');

            $.ajax({
                url: '/index.php/orders/delete',
                dataType: 'json',
                type: 'GET',
                data: {id: id},
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.reload();
                    }   
                }
            });
        }
        

    })


    // back_order
    $(document).on('click','.back_order',function(){
        var _this = $(this);
        _this.attr('disabled',true)

        if(confirm('Buyurtmani ortga qaytarmoqchimisiz ?')){
            let id = $(this).attr('order-id');

            $.ajax({
                url: '/index.php/orders/back-order',
                dataType: 'json',
                type: 'GET',
                data: {id: id},
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.reload();
                    }
                    if(response.status == 'failure_back'){
                        alert('Bu bo`limdan buyurtmani ortga qaytarib bo`lmaydi')
                    }   
                }
            });
        }
        else{
            _this.attr('disabled',false)
        }
    })


    // save_desc
    $(document).on('click','#save_desc',function(){
        let desc = $('#desc').val()
        let id = $(this).attr('data-id')

        $.ajax({
            url: '/index.php/orders/change-desc',
            dataType: 'json',
            type: 'GET',
            data: {desc: desc,id: id},
            success: function (response) {
                if (response.status == 'success') {
                    window.location.reload();
                }
                if(response.status == 'failure'){
                    alert('Xatolik! Buyurtma topilmadi!')
                }
                if(response.status == 'failure_save'){
                    alert('Xatolik! Buyurtma izohini o`zgartirilmadi!')
                }   
            }
        });
        
    })

JS;


$this->registerJs($js);
?>