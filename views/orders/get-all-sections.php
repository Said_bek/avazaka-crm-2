<?php

use kartik\select2\Select2;

?>
<div class="col-md-12" style="padding-bottom:10px;">
    <div class="row">
        <div class="col-md-11">
            <?= Select2::widget([
                'name' => 'sections['.$count.']',
                'data' => $sections,
                'size' => Select2::MEDIUM,
                'options' => [
                    'required' => true,
                ],
                'pluginOptions' => [
                ],
            ]) ?>
        </div>
        <div class="col-md-1">
            <span class="btn btn-danger btn-flat delete-this-row pull-right"><i class="fa fa-trash" aria-hidden="true"></i></span>
        </div>
    </div>
</div>