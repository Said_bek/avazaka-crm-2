<?php

use kartik\select2\Select2;

?>
<div class="col-md-12" style="padding-bottom:10px;">
    <div class="row">
        <div class="col-md-7">
            <?= Select2::widget([
                'name' => 'details['.$count.'][id]',
                'data' => $details,
                'size' => Select2::MEDIUM,
                'options' => [
                    'required' => true,
                ],
                'pluginOptions' => [
                ],
            ]) ?>
        </div>
        <div class="col-md-4">
            <input type="number" class="form-control" name="<?php echo 'details['.$count.'][count]';?>" value="1" min='1' required>
        </div>
        <div class="col-md-1">
            <span class="btn btn-danger btn-flat delete-this-row pull-right"><i class="fa fa-trash" aria-hidden="true"></i></span>
        </div>
    </div>
</div>