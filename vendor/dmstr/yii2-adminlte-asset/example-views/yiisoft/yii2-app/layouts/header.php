<?php
    use yii\helpers\Html;

?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">CRM</span><span class="logo-lg">Avazaka CRM</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <div class="pull-right" style="margin:8px 8px 0 0;">
                        <?= Html::a(
                            'Chiqish',
                            ['/index.php/site/logout'],
                            ['data-method' => 'post', 'class' => 'btn btn-primary btn-flat']
                        ) ?>
                    </div>
                </li>
                <li class="sidebarr">
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-edit"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $(".sidebarr").fadeOut("fast");
    });
</script>
