<?php

use app\models\UserModule;

$left_menu_items = [];
if (isset(Yii::$app->user->identity->id) && !empty(Yii::$app->user->identity->id)) {
    $user_modules = UserModule::find()->where(['=', 'user_id', Yii::$app->user->identity->getId()])->all();
    if (isset($user_modules) && !empty($user_modules)) {
        foreach ($user_modules as $user_module) {
            if ($user_module->status == 1) {
                if ($user_module->module_id == 1) {
                    $left_menu_items[] = [
                        'label' => 'Buyurtmalar',
                        'icon' => 'tasks',
                        'url' => '#',
                        'items' => [

                            [
                                'label' => 'Jarayondagi buyurtmalar', 'active' => Yii::$app->controller->id == 'orders', 'icon' => 'tasks', 'url' => ['index.php/orders/index']
                            ],

                            [
                                'label' => 'Buyurtmalar holati', 'active' => Yii::$app->controller->id == 'orders',  'icon' => 'bolt', 'url' => ['index.php/orders/view']
                            ],
                            [
                                'label' => 'Bitgan buyurtmalar', 'active' => Yii::$app->controller->id == 'orders',  'icon' => 'flag-checkered', 'url' => ['index.php/orders/complete']
                            ],
                            [
                                'label' => 'Buyurtma ma`lumotlari', 'active' => Yii::$app->controller->id == 'orders',  'icon' => 'file', 'url' => ['index.php/orders/order-materials']
                            ],
                            [
                                'label' => 'Majburiy fayllar', 'active' => Yii::$app->controller->id == 'required-material-order',  'icon' => 'thumb-tack', 'url' => ['index.php/required-material-order/index']
                            ],

                        ],
                    ];
                } elseif ($user_module->module_id == 2) {
                } elseif ($user_module->module_id == 3) {
                    $left_menu_items[] = ['label' => 'Foydalanuvchilar', 'active' => Yii::$app->controller->id == 'team', 'icon' => 'user-o', 'url' => ['index.php/users/index']];
                } elseif ($user_module->module_id == 4) {
                    $left_menu_items[] = ['label' => 'Bo`limlar', 'active' => Yii::$app->controller->id == 'sections',  'icon' => 'gavel', 'url' => ['index.php/sections/index']];
                } elseif ($user_module->module_id == 5) {
                    $left_menu_items[] = ['label' => 'Mijozlar', 'active' => Yii::$app->controller->id == 'clients',  'icon' => 'user-secret', 'url' => ['index.php/clients/index']];
                } elseif ($user_module->module_id == 6) {
                    $left_menu_items[] = [
                        'label' => 'Hisobotlar',
                        'icon' => 'bar-chart',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Bitgan buyurtmalar boyicha',
                                'icon' => 'bookmark-o',
                                'active' => (Yii::$app->controller->id == 'report' && Yii::$app->controller->action->id == 'index'),
                                'url' => ['index.php/report/index'],
                            ],
                            [
                                'label' => 'Buyurtmalar boyicha',
                                'icon' => 'bookmark',
                                'active' => (Yii::$app->controller->id == 'report' && Yii::$app->controller->action->id == 'active-orders'),
                                'url' => ['index.php/report/active-orders'],
                            ],
                            [
                                'label' => 'Mijoz baholari bo`yicha',
                                'icon' => 'thumbs-o-up',
                                'active' => (Yii::$app->controller->id == 'report' && Yii::$app->controller->action->id == 'feedback_client'),
                                'url' => ['index.php/report/feedback_client'],
                            ],
                            // [
                            //     'label' => 'Harakatlar',
                            //     'icon' => 'edit',
                            //     'active' => (Yii::$app->controller->id == 'events' && Yii::$app->controller->action->id == 'index'),
                            //     'url' => ['index.php/events/index'],
                            // ],
                        ],
                    ];
                } elseif ($user_module->module_id == 7) {
                    $left_menu_items[] = [
                        'label' => 'Moliya bolimi',
                        'icon' => 'money',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Pull toldirish',
                                'active' => (Yii::$app->controller->id == 'wallet' && Yii::$app->controller->action->id == 'index'),
                                'icon' => 'credit-card-alt',
                                'url' => ['index.php/wallet/index']
                            ],
                            [
                                'label' => 'Hisobot',
                                'active' => (Yii::$app->controller->id == 'wallet' && Yii::$app->controller->action->id == 'report'),
                                'icon' => 'bar-chart',
                                'url' => ['index.php/wallet/report']
                            ]
                        ],
                    ];
                } elseif ($user_module->module_id == 8) {
                    $left_menu_items[] = [
                        'label' => 'Sozlamalar',
                        'icon' => 'cog',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'kvadrat-metr',
                                'icon' => 'square-o',
                                'active' => Yii::$app->controller->id == 'settings',
                                'url' => ['index.php/settings/index'],
                            ],
                            [
                                'label' => 'Dollar kurs',
                                'icon' => 'dollar',
                                'active' => Yii::$app->controller->id == 'settings',
                                'url' => ['index.php/settings/dollar-index'],
                            ],
                            [
                                'label' => 'Modul biriktirish',
                                'icon' => 'sliders',
                                'active' => Yii::$app->controller->id == 'settings',
                                'url' => ['index.php/settings/module'],
                            ],
                        ],
                    ];
                } elseif ($user_module->module_id == 9) {
                    $left_menu_items[] = [
                        'label' => 'Savollar',
                        'icon' => 'question-circle',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Klient savollari',
                                'active' => (Yii::$app->controller->id == 'feedback-client' && Yii::$app->controller->action->id == 'index'),
                                'icon' => 'question',
                                'url' => ['index.php/feedback-client/index']
                            ],
                            [
                                'label' => 'Klient javollari',
                                'active' => (Yii::$app->controller->id == 'feedback-client' && Yii::$app->controller->action->id == 'balls'),
                                'icon' => 'question',
                                'url' => ['index.php/feedback-client/balls']
                            ]
                        ],
                    ];
                } elseif ($user_module->module_id == 10) {
                }
            }
        }
    }
}
$the_not_reg_menu = $left_menu_items;
$the_not_reg_menu[] = ['label' => 'Filiallar', 'active' => Yii::$app->controller->id == 'branch', 'icon' => 'bank', 'url' => ['index.php/branch/index']];
$the_not_reg_menu[] = ['label' => 'Kategoriyalar', 'active' => Yii::$app->controller->id == 'category', 'icon' => 'dashboard', 'url' => ['index.php/category/index']];
$the_not_reg_menu[] = ['label' => 'Detalar', 'active' => Yii::$app->controller->id == 'details', 'icon' => 'cogs', 'url' => ['index.php/details/index']];
$the_not_reg_menu[] = [
    'label' => 'Botlar',
    'icon' => 'fa fa-telegram',
    'url' => '#',
    'items' => [
        [
            'label' => 'Kroy 3D Bot',
            'icon' => 'television',
            'active' => Yii::$app->controller->id == 'bot',
            'url' => ['index.php/bot/index'],
        ],
        [
            'label' => 'Usta bot',
            'icon' => 'wrench',
            'active' => Yii::$app->controller->id == 'bot',
            'url' => ['index.php/bot/master'],
        ],
    ],
];
$left_menu_items = $the_not_reg_menu;
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => $left_menu_items,
            ]
        ) ?>

    </section>

</aside>