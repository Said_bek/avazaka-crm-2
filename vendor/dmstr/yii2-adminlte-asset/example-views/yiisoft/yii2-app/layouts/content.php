<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
use app\models\Orders;
use app\models\Users;
use app\models\Events;

?>
<div class="content-wrapper">
    <section class="content-box">
        <div class="box">
            <div class="box-body">
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </section>
</div>

<style>
    .side-bar-right {
        height: 100vh;
        overflow-y: auto;
    }
</style>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark side-bar-right">
    <!-- Create the tabs -->
<!--    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">-->
<!--        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-gears"></i> Menu</a></li>-->
<!--    </ul>-->
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
<!--        class="tab-pane"-->
        <div id="control-sidebar-home-tab">
            <h1 class="control-sidebar-heading" style="padding: 0; margin-top: 5px; font-size: 20px;"><i class="fa fa-align-left" style="font-size: 12px"></i> Harakatlar</h1>
            <ul class='control-sidebar-menu'>
                <?php
                    $selectEvent = Events::find()->orderBy(['id' => SORT_DESC])->all();
                    foreach ($selectEvent as $value) {
                        $user_id = $value->user_id;
                        $eventUser = $value->event;
                        $eventTitle = $value->title;
                        ?>
                        <li>
                            <a href='javascript::;'>
                                <i class="menu-icon fa fa-user bg-green"></i>

                                <div class="menu-info">
                                    <?php
                                        $selectUser = Users::find()->where(['id' => $user_id])->one();
                                        $user_name = ((isset($selectUser))? $selectUser->username : '');
                                    ?>
                                    <h4 class="control-sidebar-subheading"><?php echo $user_name; ?></h4>

                                    <p> <?php echo $eventTitle; ?> </p>
                                    <p> <?php echo $eventUser; ?> </p>
                                </div>
                            </a>
                        </li>
                        <?php
                    }
                ?>
            </ul>
            <!-- /.control-sidebar-menu -->
        </div>
        <!-- /.tab-pane -->
    </div>
</aside><!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class='control-sidebar-bg'></div>