<?php

namespace app\controllers;

use app\models\ClientDebtors;
use app\models\Clients;
use app\models\ClientWallet;
use Yii;
use app\models\WalletPay;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class WalletController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    //Pull toldirsh page ochis
    public function actionIndex()
    {
        //Default settings
        if (true) {
            $model = new WalletPay();
            $clients = Clients::find()->all();
        }

        //Change varibles
        if (true) {
            $time_arr = [];
            if (isset($clients) && !empty($clients)) {
                foreach ($clients as $client) {
                    $time_arr[$client->id] = base64_decode($client->full_name);
                }
            }
            $clients = $time_arr;
        }

        return $this->render('index', [
            'model' => $model,
            'clients' => $clients,
        ]);
    }

    //Pull toldirsh ajax
    public function actionPay()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //Change varible
            if (true) {
                $status = '';
                $message = [];
                $clients = Clients::find()->all();
            }
            //Post controle
            if (true) {
                if (isset($_POST['WalletPay']) && !empty($_POST['WalletPay'])) {
                    $wallet_post = $_POST['WalletPay'];
                    // pre($wallet_post);

                    //validate
                    if (true) {
                        if (isset($wallet_post['client_id']) && (!empty($wallet_post['client_id']))) {
                            if (isset($clients) && !empty($clients)) {
                                $is_have_db_client = false;
                                foreach ($clients as $client) {
                                    if ($client->id == $wallet_post['client_id']) {
                                        $is_have_db_client = true;
                                        break;
                                    }
                                }

                                if (!$is_have_db_client) {
                                    $message[] = 'Siz qoshgan mijoz yoq';
                                }
                            } else {
                                $message[] = 'Mijozlar qoshing';
                            }
                        } else {
                            $message[] = 'Mijozlar tanlang';
                        }

                        if (isset($wallet_post['price']) && (!empty($wallet_post['price']))) {
                            if ($wallet_post['price'] > 0) {
                                if (!is_numeric($wallet_post['price'])) {
                                    $message[] = 'Narx son bolish kerak';
                                }
                            } else {
                                $message[] = 'Narx 0 dan kotta bolish shart';
                            }
                        } else {
                            $message[] = 'Narx yoq';
                        }

                        if (isset($wallet_post['price_type']) && (!empty($wallet_post['price_type']))) {
                            if (!($wallet_post['price_type'] == 1 || $wallet_post['price_type'] == 2)) {
                                $message[] = 'Berilgan narx tanlang';
                            }
                        } else {
                            $message[] = 'Narx turini tanlang';
                        }
                    }

                    //Send error
                    if (true) {
                        if (isset($message) && !empty($message)) {
                            $time_arr = [];
                            $time_arr[] = 'Tolidirshda xato bolgan sabali bekor qilindi';
                            foreach ($message as $error) {
                                $time_arr[] = $error;
                            }
                            $message = $time_arr;

                            return  [
                                'status' => 'error',
                                'message' => $message
                            ];
                        }
                    }

                    //Save
                    if (true) {
                        $message = [];
                        $transaction = Yii::$app->db->beginTransaction();
                        try {
                            $wallet_log = new WalletPay;
                            $wallet_log->user_id = Yii::$app->user->identity->id;
                            $wallet_log->client_id = $wallet_post['client_id'];
                            $wallet_log->price = $wallet_post['price'];
                            $wallet_log->price_type = $wallet_post['price_type'];
                            $wallet_log->created_date = date('Y-m-d H:i:s');

                            if (!$wallet_log->save()) {
                                $transaction->rollBack();
                                return [
                                    'status' => 'error',
                                    'message' => ['Toldirishda xato bor sababli bekor qilindi']
                                ];
                            } else {
                                //Get wallet
                                if (true) {
                                    $client_wallet = ClientWallet::find()->where(['=', 'client_id', $wallet_log->client_id])->one();
                                    if (isset($client_wallet) && !empty($client_wallet)) {
                                        //add to wallet
                                        if (true) {
                                            if ($wallet_log->price_type == 1) {
                                                //som
                                                $client_wallet->som += $wallet_log->price;
                                            } else {
                                                //dollar
                                                $client_wallet->dollar += $wallet_log->price;
                                            }

                                            $client_wallet->wallet_status = 1;
                                            $client_wallet->save();
                                        }
                                    } else {
                                        //create wallet
                                        $client_wallet = new ClientWallet();
                                        $client_wallet->client_id = $wallet_log->client_id;
                                        $client_wallet->som = 0;
                                        $client_wallet->dollar = 0;
                                        $client_wallet->wallet_status = 0;
                                        $client_wallet->save();

                                        //Changes
                                        if (true) {
                                            if ($wallet_log->price_type == 1) {
                                                //som
                                                $client_wallet->som += $wallet_log->price;
                                            } else {
                                                //dollar
                                                $client_wallet->dollar += $wallet_log->price;
                                            }

                                            $client_wallet->wallet_status = 1;
                                            $client_wallet->save();
                                        }
                                    }
                                }


                                //Check debtors
                                if (true) {
                                    //debtors varibles
                                    if (true) {
                                        $close_debt_count = 0;
                                        $have_debt_count = 0;
                                        $is_have_debt = false;
                                        $client_debtors = ClientDebtors::find()
                                            ->where(['=', 'client_id', $wallet_log->client_id])
                                            ->andWhere(['=', 'debt_status', 0]);
                                    }


                                    //add condition to price_type
                                    if (true) {
                                        if ($wallet_log->price_type == 1) {
                                            //som
                                            $client_debtors = $client_debtors
                                                ->andWhere(['=', 'dollar', 0])
                                                ->andWhere(['>', 'som', 0]);
                                        } else {
                                            //dollar
                                            $client_debtors = $client_debtors
                                                ->andWhere(['>', 'dollar', 0])
                                                ->andWhere(['=', 'som', 0]);
                                        }
                                    }

                                    $client_debtors = $client_debtors->all();

                                    //debtors control
                                    if (true) {
                                        if (isset($client_debtors) && !empty($client_debtors)) {
                                            $have_debt_count = count($client_debtors);
                                            $is_have_debt = true;
                                            foreach ($client_debtors as $client_debtor) {
                                                if ($wallet_log->price_type == 1) {
                                                    $wallet_price = $client_wallet->som;
                                                    $debtors_price = $client_debtor->som;
                                                } else {
                                                    $wallet_price = $client_wallet->dollar;
                                                    $debtors_price = $client_debtor->dollar;
                                                }

                                                // pre($wallet_price - $debtors_price);
                                                if ($wallet_price - $debtors_price  > 0) {

                                                    //debt yopilidi
                                                    $have_debt_count--;
                                                    $close_debt_count++;
                                                    $client_debtor->debt_status = 1;
                                                    $client_debtor->save();

                                                    if ($wallet_log->price_type == 1) {
                                                        $client_wallet->som = $client_wallet->som - $debtors_price;
                                                    } else {
                                                        $client_wallet->dollar = $client_wallet->dollar - $debtors_price;
                                                    }
                                                } elseif ($wallet_price - $debtors_price == 0) {
                                                    $have_debt_count--;
                                                    $close_debt_count++;

                                                    //debt yopilidi va break
                                                    $client_debtor->debt_status = 1;
                                                    $client_debtor->save();
                                                    if ($wallet_log->price_type == 1) {
                                                        $client_wallet->som = 0;
                                                    } else {
                                                        $client_wallet->dollar = 0;
                                                    }
                                                    break;
                                                } else {
                                                    //debt katta
                                                    if ($wallet_log->price_type == 1) {
                                                        $client_debtor->som = $client_debtor->som - $client_wallet->som;
                                                        $client_debtor->save();
                                                        $client_wallet->som = 0;
                                                    } else {
                                                        $client_debtor->dollar = $client_debtor->dollar - $client_wallet->dollar;
                                                        $client_debtor->save();
                                                        $client_wallet->dollar = 0;
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                //Save wallet
                                if (true) {
                                    //Wallet saving
                                    if (true) {
                                        if ($client_wallet->som < 0) {
                                            $client_wallet->som = 0;
                                        }
                                        if ($client_wallet->dollar < 0) {
                                            $client_wallet->dollar = 0;
                                        }

                                        if ($client_wallet->som == 0 && $client_wallet->dollar == 0) {
                                            $client_wallet->wallet_status = 0;
                                        }

                                        $client_wallet->save();
                                    }
                                    $transaction->commit();

                                    //writing message
                                    if (true) {
                                        if ($is_have_debt) {
                                            //yopilidi
                                            if ($close_debt_count > 0 && $have_debt_count == 0) {
                                                //Shuncha qarzlar yopldi va qolmadi
                                                $message[] = 'Shuncha: ' . $close_debt_count . ' qarzlar yopldi va qarz qolmadi';
                                            } elseif ($close_debt_count == 0 && $have_debt_count > 0) {
                                                //Qarzdan ayrildi va shuncha qarz qoldi 
                                                $message[] = 'Tushgan narx qarzdan ayrildi va shuncha: ' . $have_debt_count . ' qarz qoldi';
                                            } elseif ($close_debt_count > 0 && $have_debt_count > 0) {
                                                //Shuncha qarz yopildi va shuncha qarz qoldi
                                                $message[] = 'Shuncha: ' . $close_debt_count . ' qarzlar yopldi va shuncha: ' . $have_debt_count . ' qarz qoldi';
                                            }
                                        } else {
                                            //toldirilid
                                            $message[] = 'Pull toldish muvafiqiyatli otdi';
                                        }

                                        return [
                                            'status' => 'success',
                                            'message' => $message
                                        ];
                                    }
                                }

                            }
                        } catch (\Exception $th) {
                            $transaction->rollBack();
                            return [
                                'status' => 'error',
                                'message' => ['Toldirishda xato bor sababli bekor qilindi']
                            ];
                        }
                    }
                } else {
                    return [
                        'status' => 'error',
                        'message' => ['malumotlar notogri ketgan']
                    ];
                }
            }

            return [
                'status' => $status,
                'message' => $message
            ];
        }
    }

    public function actionReport()
    {
        $wallet_pays = WalletPay::find()->orderBy(['created_date' => SORT_DESC])->all();
        $clients = Clients::find()->all();
        $filter_model = new WalletPay;

        //Client obj to array
        if (true) {
            $time_arr = [];
            if (isset($clients) && !empty($clients)) {
                foreach ($clients as $client) {
                    $time_arr[$client->id] = base64_decode($client->full_name);
                }
            }
            $clients = $time_arr;
        }

        return $this->render('report', [
            'wallet_pays' => $wallet_pays,
            'clients' => $clients,
            'model' => $filter_model,
        ]);
    }

    public function actionReportFilter()
    {
    }

    public function actionReportExcel()
    {
    }


    protected function findModel($id)
    {
        if (($model = WalletPay::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
