<?php

namespace app\controllers;

use app\models\Clients;
use Yii;
use app\models\Sections;
use app\models\Orders;
use app\models\FeedbackUser;
use app\models\FeedbackClient;
use app\models\SectionOrdersControl;
use yii\data\Pagination;


class ReportController extends \yii\web\Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
    }

    public function actionIndex()
    {
        $filter_model = new Orders;
        $orders = Orders::find()
            ->where(['=', 'status', 0])
            ->orderBy(['created_date' => SORT_DESC]);
        if (isset($_POST['Orders']) && !empty($_POST['Orders'])) {
            //options
            if (true) {
                $filter_have = true;
                $filter = $_POST['Orders'];
            }


            //client
            if (true) {
                if (isset($filter['client_id']) && !empty($filter['client_id'])) {
                    $orders = $orders->andWhere(['in', 'client_id', $filter['client_id']]);
                } else {
                    $orders = $orders->andWhere(['in', 'client_id', []]);
                }
            }

            //id
            if (true) {
                if (isset($filter['id']) && !empty($filter['id'])) {
                    $filter_model->id = $filter['id'];
                    $orders = $orders->andWhere(['=', 'id', $filter['id']]);
                }
            }

            //created_date
            if (true) {
                if (isset($filter['created_date']) && !empty($filter['created_date'])) {
                    $filter_model->created_date = $filter['created_date'];
                    $orders = $orders->andWhere(['>=', 'created_date', date('Y-m-d', strtotime($filter['created_date'])) . ' 00:00:00']);
                    $orders = $orders->andWhere(['<=', 'created_date', date('Y-m-d', strtotime($filter['created_date'])) . ' 23:59:59']);
                }
            }

            //end_date
            if (true) {
                if (isset($filter['end_date']) && !empty($filter['end_date'])) {
                    $filter_model->end_date = $filter['end_date'];
                    $orders = $orders->andWhere(['>=', 'end_date', date('Y-m-d', strtotime($filter['end_date'])) . ' 00:00:00']);
                    $orders = $orders->andWhere(['<=', 'end_date', date('Y-m-d', strtotime($filter['end_date'])) . ' 23:59:59']);
                }
            }

            //price_type 
            if (true) {
                if (isset($filter['price_type']) && !empty($filter['price_type'])) {
                    $filter_model->price_type = $filter['price_type'];
                    $price_type = $filter['price_type'];
                    if (isset($price_type['dollar'], $price_type['som']) && (!empty($price_type['dollar']) && !empty($price_type['som']))) {
                    } elseif (isset($price_type['dollar']) && !empty($price_type['dollar'])) {
                        $orders = $orders->andWhere(['=', 'price_type', $price_type['dollar']]);
                    } elseif (isset($price_type['som']) && !empty($price_type['som'])) {
                        $orders = $orders->andWhere(['=', 'price_type', $price_type['som']]);
                    } else {
                        $orders = $orders->andWhere(['!=', 'price_type', $price_type['dollar']]);
                        $orders = $orders->andWhere(['!=', 'price_type', $price_type['som']]);
                    }
                } else {
                    $orders = $orders->andWhere(['!=', 'price_type', 1]);
                    $orders = $orders->andWhere(['!=', 'price_type', 2]);
                }
            }
        } else {
            $filter = '';
            $filter_have = false;
        }
        $orders = $orders->all();


        $exit_dates = SectionOrdersControl::find()
            ->where(['=', 'section_id', 0])
            ->andWhere(['not', ['exit_date' => null]])
            ->all();

        $clients = Clients::find()->all();

        $time_arr = [];
        if (isset($clients) && !empty($clients)) {
            foreach ($clients as $client) {
                $time_arr[$client->id] = base64_decode($client->full_name);
            }
        }
        $clients = $time_arr;

        return $this->render('index', [
            'orders' => $orders,
            'clients' => $clients,
            'filter_model' => $filter_model,
            'exit_dates' => $exit_dates,
            'filter_have' => $filter_have,
            'filter' => $filter,
        ]);
    }

    public function actionFeedback_user()
    {
        $orders = Orders::find()
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $feedback_user = FeedbackUser::find()->orderBy(['id' => SORT_ASC])->all();

        return $this->render('feedback_user', [
            'orders' => $orders,
            'feedback_user' => $feedback_user,
        ]);
    }

    public function actionFeedback_client()
    {
        $orders = Orders::find();
        $pages = new Pagination(['totalCount' => $orders->count(), 'pageSize' => 20]);
        $orders = $orders->offset($pages->offset)
            ->orderBy(['id' => SORT_DESC])
            ->limit($pages->limit)
            ->all();
        $feedback_client = FeedbackClient::find()->orderBy(['id' => SORT_ASC])->all();

        return $this->render('feedback_client', [
            'orders' => $orders,
            'feedback_client' => $feedback_client,
            'pages' => $pages,
        ]);
    }

    public function actionActiveOrders()
    {

        $filter_model = new Orders;
        $sections_controls = SectionOrdersControl::find()->orderBy(['exit_date' => SORT_ASC])->all();
        $orders = Orders::find()
            ->where(['=', 'status', 1])
            ->orderBy(['created_date' => SORT_DESC]);
        if (isset($_POST['Orders']) && !empty($_POST['Orders'])) {
            //options
            if (true) {
                $filter_have = true;
                $filter = $_POST['Orders'];
            }

            //client
            if (true) {
                if (isset($filter['client_id']) && !empty($filter['client_id'])) {
                    $orders = $orders->andWhere(['in', 'client_id', $filter['client_id']]);
                } else {
                    $orders = $orders->andWhere(['in', 'client_id', []]);
                }
            }

            //id
            if (true) {
                if (isset($filter['id']) && !empty($filter['id'])) {
                    $filter_model->id = $filter['id'];
                    $orders = $orders->andWhere(['=', 'id', $filter['id']]);
                }
            }

            //created_date
            if (true) {
                if (isset($filter['created_date']) && !empty($filter['created_date'])) {
                    $filter_model->created_date = $filter['created_date'];
                    $orders = $orders->andWhere(['>=', 'created_date', date('Y-m-d', strtotime($filter['created_date'])) . ' 00:00:00']);
                    $orders = $orders->andWhere(['<=', 'created_date', date('Y-m-d', strtotime($filter['created_date'])) . ' 23:59:59']);
                }
            }

            //price_type 
            if (true) {
                if (isset($filter['price_type']) && !empty($filter['price_type'])) {
                    $filter_model->price_type = $filter['price_type'];
                    $price_type = $filter['price_type'];
                    if (isset($price_type['dollar'], $price_type['som']) && (!empty($price_type['dollar']) && !empty($price_type['som']))) {
                    } elseif (isset($price_type['dollar']) && !empty($price_type['dollar'])) {
                        $orders = $orders->andWhere(['=', 'price_type', $price_type['dollar']]);
                    } elseif (isset($price_type['som']) && !empty($price_type['som'])) {
                        $orders = $orders->andWhere(['=', 'price_type', $price_type['som']]);
                    } else {
                        $orders = $orders->andWhere(['!=', 'price_type', $price_type['dollar']]);
                        $orders = $orders->andWhere(['!=', 'price_type', $price_type['som']]);
                    }
                } else {
                    $orders = $orders->andWhere(['!=', 'price_type', 1]);
                    $orders = $orders->andWhere(['!=', 'price_type', 2]);
                }
            }

            //last options
            if (true) {
                $orders = $orders->all();
                if (isset($orders) && !empty($orders)) {
                    if (isset($filter['section_id']) && !empty($filter['section_id'])) {
                        $time_arr = [];
                        foreach ($orders as $order) {
                            $sections_id = true;
                            $sections_control_date = date('Y-m-d H:i:s', strtotime(0));
                            if (isset($sections_controls) && !empty($sections_controls)) {
                                foreach ($sections_controls as $sections_control) {
                                    if ($sections_control->order_id == $order->id) {
                                        if ($sections_control->exit_date == null) {
                                            $sections_id  = $sections_control->section_id;
                                            break;
                                        } elseif (strtotime($sections_control->exit_date) > strtotime($sections_control_date)) {
                                            $sections_id  = $sections_control->section_id;
                                            $sections_control_date = $sections_control->exit_date;
                                        }
                                    }
                                }
                            }
                            if (is_int($sections_id)) {
                                foreach ($filter['section_id'] as $section_id_filter) {
                                    if ($section_id_filter == $sections_id) {
                                        $time_arr[] = $order;
                                        break;
                                    }
                                }
                            }
                        }
                        $orders = $time_arr;
                    }else {
                        $orders = [];
                    }
                }
            }
        } else {
            $filter = '';
            $filter_have = false;
            $orders = $orders->all();
        }

        //client arr
        if (true) {
            $clients = Clients::find()->all();
            $time_arr = [];
            if (isset($clients) && !empty($clients)) {
                foreach ($clients as $client) {
                    $time_arr[$client->id] = base64_decode($client->full_name);
                }
            }
            $clients = $time_arr;
        }

        //sections arr
        if (true) {
            $sections = Sections::find()->all();
            $time_arr = [];
            $time_arr[0] = 'Buyurtma boshlanmagan';
            if (isset($sections) && !empty($sections)) {
                foreach ($sections as $section) {
                    $time_arr[$section->id] = $section->title;
                }
            }
            $sections = $time_arr;
        }


        return $this->render('active-orders', [
            'orders' => $orders,
            'clients' => $clients,
            'filter_model' => $filter_model,
            'sections_controls' => $sections_controls,
            'filter_have' => $filter_have,
            'filter' => $filter,
            'sections' => $sections,
        ]);
    }
}
