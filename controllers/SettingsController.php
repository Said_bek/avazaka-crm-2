<?php

namespace app\controllers;

use Yii;
use app\models\SquareSettings;
use app\models\CourseSettings;
use app\models\User;
use app\models\Users;
use app\models\RoleBaseModule;
use app\models\Modules;
use app\models\UserModule;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class SettingsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $query = SquareSettings::find()->orderBy(['end_date' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false
        ]);

        $model = $query->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new SquareSettings();

        if ($model->load(Yii::$app->request->post())) {
            if (isset($model->limit) && !empty($model->limit) && is_numeric($model->limit)) {
                $last_limits = SquareSettings::find()->where('end_date IS NULL')->all();
                if (isset($last_limits) && !empty($last_limits)) {
                    foreach ($last_limits as $limit) {
                        $limit->end_date = date('Y-m-d H:i:s', strtotime('-1 minute'));
                        $limit->save();
                    }
                }
                $model->start_date = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['/index.php/settings/index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = SquareSettings::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDollarIndex()
    {
        $query = CourseSettings::find()->orderBy(['end_date' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false
        ]);

        $model = $query->all();

        return $this->render('dollar-index', [
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    public function actionDollarCreate()
    {
        $model = new CourseSettings();

        if ($model->load(Yii::$app->request->post())) {
            if (isset($model->price) && !empty($model->price) && is_numeric($model->price)) {
                $last_limits = CourseSettings::find()->where('end_date IS NULL')->all();
                if (isset($last_limits) && !empty($last_limits)) {
                    foreach ($last_limits as $limit) {
                        $limit->end_date = date('Y-m-d H:i:s', strtotime('-1 minute'));
                        $limit->save();
                    }
                }
                $model->start_date = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['/index.php/settings/dollar-index']);
            }
        }

        return $this->render('dollar-create', [
            'model' => $model,
        ]);
    }

    public function actionDeleteSquare($id)
    {
        $model = SquareSettings::find()->where('end_date is null')->one();
        if (isset($model) && !empty($model)) {
            $model->delete();
        }
        $new_model = SquareSettings::find()->orderBy(['end_date' => SORT_DESC])->one();
        $new_model->end_date = null;
        $new_model->save();
        return $this->redirect('index');
    }

    public function actionDeleteDollar($id)
    {
        $model = CourseSettings::find()->where('end_date is null')->one();
        if (isset($model) && !empty($model)) {
            $model->delete();
        }
        $new_model = CourseSettings::find()->orderBy(['end_date' => SORT_DESC])->one();
        $new_model->end_date = null;
        $new_model->save();
        return $this->redirect('dollar-index');
    }

    public function actionModule()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Users::find(),
            'sort' => false
        ]);

        $sql_select = 'SELECT 
            um.id AS user_module_id,
            um.user_id AS user_id,
            um.module_id AS module_id,
            um."status" AS status,
            mo.name AS module_name,
            mo.module_code AS module_code
        FROM user_module AS um
        LEFT JOIN modules AS mo
        ON mo.id = um.module_id
        ORDER BY user_id,mo.id';
        $users_module = Yii::$app->db->createCommand($sql_select)->queryAll();

        $timer_arr = [];
        if (isset($users_module) && !empty($users_module)) {
            foreach ($users_module as $user_module) {
                $timer_arr[$user_module['user_id']][] = $user_module;
            }
        }
        $users_module = $timer_arr;

        return $this->render('module-index', [
            'dataProvider' => $dataProvider,
            'users_module' => $users_module,
        ]);
    }

    public function actionModuleUpdate($id)
    {
        if (isset($_POST) && !empty($_POST)) {
            $user_modules = UserModule::find()->where('user_id = :user_id')->addParams([':user_id' => $id])->all();
            foreach ($user_modules as $user_module) {
                $user_module->status = 0;
                $user_module->save();
            }
            if (isset($_POST['module']) && !empty($_POST['module'])) {
                foreach ($_POST['module'] as $active_module_id) {
                    $user_module = UserModule::find()->where('user_id = :user_id')->andWhere('module_id = :module_id')->addParams([':user_id' => $id, ':module_id' => $active_module_id])->one();
                    $user_module->status = 1;
                    $user_module->save();
                }
                $user_modules = UserModule::find()->where('user_id = :user_id')->addParams([':user_id' => $id])->all();
            }
            return $this->redirect(['/index.php/settings/module-view','id' => $id]);
        }

        $sql_select = 'SELECT um.module_id FROM user_module AS um WHERE um.user_id = :user_id AND um.status = :status ORDER BY um.module_id';
        $user_modules = Yii::$app->db->createCommand($sql_select)
            ->bindValue(':user_id', $id)
            ->bindValue(':status', 1)
            ->queryAll();
        $time_arr = [];
        if (isset($user_modules) && !empty($user_modules)) {
            foreach ($user_modules as $user_module) {
                $time_arr[] = $user_module['module_id'];
            }
        }
        $user_modules = $time_arr;

        $sql_select = 'SELECT * FROM modules AS m ORDER BY m.id';
        $modules =  Yii::$app->db->createCommand($sql_select)->queryAll();
        $time_arr = [];
        if (isset($modules) && !empty($modules)) {
            foreach ($modules as $module) {
                $time_arr[$module['id']] = $module['name'];
            }
        }
        $modules = $time_arr;

        $user = Users::find()->where(['=', 'user_id', $id])->one();

        return $this->render('module-update', [
            'user_modules' => $user_modules,
            'modules' => $modules,
            'user' => $user,
        ]);
    }

    public function actionModuleView($id)
    {
        $sql_select = 'SELECT um.module_id,um.status FROM user_module AS um WHERE um.user_id = :user_id ORDER BY um.module_id';
        $user_modules = Yii::$app->db->createCommand($sql_select)
            ->bindValue(':user_id', $id)
            ->queryAll();

        $sql_select = 'SELECT * FROM modules AS m ORDER BY m.id';
        $modules =  Yii::$app->db->createCommand($sql_select)->queryAll();

        $user = Users::find()->where(['=', 'user_id', $id])->one();

        return $this->render('module-view', [
            'user_modules' => $user_modules,
            'modules' => $modules,
            'user' => $user,
        ]);
    }
}
