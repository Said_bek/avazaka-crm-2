<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\User;
use app\models\UserModule;
use app\models\Branch;
use app\models\AuthItem;
use app\models\UsersBranch;
use app\models\RoleBaseModule;
use app\models\Modules;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class UsersController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Users::find()->orderBy(['type' => SORT_ASC]),
            'pagination' => false,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Users();
        $branchs = Branch::find()->all();

        $time_arr = [];
        if (isset($branchs) && !empty($branchs)) {
            foreach ($branchs as $branch) {
                $time_arr[$branch->id] = $branch->title;
            }
        }
        $branchs = $time_arr;

        $user_branchs = [];

        $roles = AuthItem::find()->all();
        $time_arr = [];
        if (isset($roles) && !empty($roles)) {
            foreach ($roles as $role) {
                $time_arr[] = $role->name;
            }
        }
        $roles = $time_arr;

        if ($model->load(Yii::$app->request->post())) {
            $model->status = 0;

            if (!isset($_POST['Users']['user_branch'])) {
                Yii::$app->session->setFlash('danger', "Fililal tanlamadingiz.");
                return $this->render('create', [
                    'model' => $model,
                    'branchs' => $branchs,
                    'user_branchs' => $user_branchs,
                    'roles' => $roles,
                ]);
            }

            $find = Users::find()
                ->where([
                    'phone_number' => $model->phone_number
                ])
                ->one();


            if (empty($find)) {
                $transaction = Yii::$app->db->beginTransaction();
                try {

                    $user = new User();
                    $user->username = $model->phone_number;
                    $user->password = sha1(123);
                    $user->status = 1;

                    if ($user->save()) {

                        $model->user_id = $user->id;
                        // $model->type = 3;
                        if ($model->save()) {
                            //                          START ADD EVENT
                            $user_id = Yii::$app->user->id;

                            $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                            $userId = $selectUsers->id;

                            eventUser($userId, date('Y-m-d H:i:s'), $model->second_name, "User qo'shildi", 'Foydalanuvchilar');

                            //
                        }
                        $auth = Yii::$app->authManager;

                        $user_role = $model->type;

                        // role
                        if (true) {
                            if (($user_role) == 0) {
                                $role = "Admin";
                            } elseif (($user_role) == 1) {
                                $role = "Seh boshlig’i";
                            } elseif (($user_role) == 2) {
                                $role = "Sotuv menejeri";
                            } elseif (($user_role) == 3) {
                                $role = "Brigadir";
                            } elseif (($user_role) == 4) {
                                $role = "Menejer";
                            } elseif (($user_role) == 5) {
                                $role = "Buxgalter";
                            }
                        }

                        $authorRole = $auth->getRole($role);
                        $auth->assign($authorRole, $user->id);

                        $modules = Modules::find()->all();
                        if (isset($modules) && !empty($modules)) {
                            foreach ($modules as $module) {
                                $user_module = new UserModule();
                                $user_module->user_id = $user->id;
                                $user_module->module_id = $module->id;
                                $user_module->status = 0;
                                $user_module->save();
                            }
                        }

                        $role_base_modules = RoleBaseModule::find()->where('role = :role')->addParams([':role' => $role])->all();
                        if (isset($role_base_modules) && !empty($role_base_modules)) {
                            foreach ($role_base_modules as $role_base_module) {
                                $user_module = UserModule::find()->where('module_id = :module_id')->andWhere('user_id = :user_id')->addParams([':module_id' => $role_base_module['module_id'], ':user_id' => $user->id])->one();
                                $user_module->status = 1;
                                $user_module->save();
                            }
                        }

                        if (!empty($_POST['Users']['user_branch'])) {
                            foreach ($_POST['Users']['user_branch'] as $key => $value) {
                                $user_branch = new UsersBranch();
                                $user_branch->user_id = $model->id;
                                $user_branch->branch_id = $value;
                                $user_branch->save();
                            }
                        }

                        $transaction->commit();
                        return $this->redirect(['/index.php/users/view', 'id' => $model->id]);
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;

                    Yii::$app->session->setFlash('danger', "Ma`lumotlar saqlashda xatolik !");
                    return $this->render('create', [
                        'model' => $model,
                        'branchs' => $branchs,
                        'user_branchs' => $user_branchs,
                        'roles' => $roles,
                    ]);
                }
            } else {
                Yii::$app->session->setFlash('danger', "Bunday raqamli foydalanuvchi platformada mavjud.");
                return $this->render('create', [
                    'model' => $model,
                    'branchs' => $branchs,
                    'user_branchs' => $user_branchs,
                    'roles' => $roles,
                ]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'branchs' => $branchs,
            'user_branchs' => $user_branchs,
            'roles' => $roles,
        ]);
    }

    public function actionUpdate($id)
    {
        if (true) {

            $model = $this->findModel($id);
            $branchs = Branch::find()->all();

            $time_arr = [];
            if (isset($branchs) && !empty($branchs)) {
                foreach ($branchs as $branch) {
                    $time_arr[$branch->id] = $branch->title;
                }
            }
            $branchs = $time_arr;

            $user_branchs = [];

            $roles = AuthItem::find()->all();
            $time_arr = [];
            if (isset($roles) && !empty($roles)) {
                foreach ($roles as $role) {
                    $time_arr[] = $role->name;
                }
            }
            $roles = $time_arr;

            $user_branchs = UsersBranch::find()->where('user_id = :user_id')->addParams([':user_id' => $id])->all();
            $time_arr = [];
            if (isset($user_branchs) && !empty($user_branchs)) {
                foreach ($user_branchs as $user_branch) {
                    $time_arr[] = $user_branch->branch_id;
                }
            }
            $user_branchs = $time_arr;
        }

        //Update
        if ($model->load(Yii::$app->request->post())) {

            if (!isset($_POST['Users']['user_branch'])) {
                Yii::$app->session->setFlash('danger', "Fililal tanlamadingiz.");
                return $this->render('update', [
                    'model' => $model,
                    'branchs' => $branchs,
                    'roles' => $roles,
                    'user_branchs' => $user_branchs,
                ]);
            }

            $old_phone = $model->phone_number;
            if ($model->save()) {
                if ($model->phone_number != $old_phone) {
                    $user_model = User::find()->where(['=','id',$model->user_id])->one();
                    if (isset($user_model) && !empty($user_model)) {
                        $user_model->username = $model->phone_number;
                    }
                }
                if (!empty($_POST['Users']['user_branch'])) {
                    $delete_user_branchs = UsersBranch::find()->where(['user_id' => $id])->all();

                    if (!empty($delete_user_branchs)) {
                        foreach ($delete_user_branchs as $delete_user_branch) {
                            $delete_user_branch->delete();
                        }
                    }

                    foreach ($_POST['Users']['user_branch'] as $branch_id) {
                        $user_branch = new UsersBranch();
                        $user_branch->user_id = $model->id;
                        $user_branch->branch_id = $branch_id;
                        $user_branch->save();
                    }
                }


                $auth = Yii::$app->authManager;
                $user_role = $model->type;

                if (true) {
                    if (($user_role) == 0) {
                        $role = "Admin";
                    } elseif (($user_role) == 1) {
                        $role = "Seh boshlig’i";
                    } elseif (($user_role) == 2) {
                        $role = "Sotuv menejeri";
                    } elseif (($user_role) == 3) {
                        $role = "Brigadir";
                    } elseif (($user_role) == 4) {
                        $role = "Menejer";
                    } elseif (($user_role) == 5) {
                        $role = "Buxgalter";
                    }
                }

                $user_modules = UserModule::find()->where('user_id = :user_id')->addParams([':user_id' => $model->user_id])->all();
                if (isset($user_modules) && !empty($user_modules)) {
                    foreach ($user_modules as $user_module) {
                        $user_module->status = 0;
                        $user_module->save();
                    }
                }

                $authorRole = $auth->getRole($role);

                $role_base_modules = RoleBaseModule::find()->where('role = :role')->addParams([':role' => $role])->all();
                if (isset($role_base_modules) && !empty($role_base_modules)) {
                    foreach ($role_base_modules as $role_base_module) {
                        $user_module = UserModule::find()->where('module_id = :module_id')->andWhere('user_id = :user_id')->addParams([':module_id' => $role_base_module['module_id'], ':user_id' => $model->user_id])->one();
                        $user_module->status = 1;
                        $user_module->save();
                    }
                }

                return $this->redirect(['/index.php/users/view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'branchs' => $branchs,
            'roles' => $roles,
            'user_branchs' => $user_branchs,
        ]);
    }

    public function actionDelete($id)
    {
        $user = User::findOne($this->findModel($id)->user_id);

        if (isset($user)) {
            if ($user->delete()) {
                $this->findModel($id)->delete();
                Yii::$app->session->setFlash('success', "Foydalanuvchi muvaffaqiyatli o`chirildi.");
            }
        } else {
            Yii::$app->session->setFlash('danger', "Foydalanuvchini  o`chirishda xatolik!.");
        }




        return $this->redirect(['index.php/users/index']);
    }

    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
