<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\UserModule;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            // 'access' => [
            //     'class' => AccessControl::className(),
            //     'only' => ['logout'],
            //     // 'rules' => [
            //     //     [
            //     //         'actions' => ['logout'],
            //     //         'allow' => true,
            //     //         'roles' => ['@'],
            //     //     ],
            //     // ],
            // ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'logout' => ['post'],
            //     ],
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/index.php/site/login');
        } else if (Yii::$app->user->identity->status == 1) {
            $user_modules = UserModule::find()->where(['=','user_id',Yii::$app->user->getId()])->all();
            if (isset($user_modules) && !empty($user_modules)) {
                $user_modules_array = [];
                foreach ($user_modules as $user_module) {
                    $user_modules_array[$user_module->module_id] = $user_module->status;
                }
            }else {
                Yii::$app->user->logout();
                return $this->redirect('/index.php/site/login');
            }
            $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
            $role = '';
            if (is_array($roles) && !empty($roles)) {
                $role = array_shift($roles)->name;
            }
            if ($role == "Admin") {
                if ($user_modules_array[1] == 1) {
                    return $this->redirect('/index.php/orders/index');
                }else {
                    return $this->redirect('/index.php/orders/index');
                    // return $this->redirect('/index.php/user-setting/index');
                }
            } else if ($role == "Seh boshlig’i") {
                return $this->redirect('/index.php/orders/index');
            } else if ($role == "Sotuv menejeri") {
                return $this->redirect('/index.php/orders/index');
            } else if ($role == "Brigadir") {
                return $this->redirect('/index.php/orders/index');
            } else if ($role == "Menejer") {
                return $this->redirect('/index.php/orders/index');
            } else if ($role == "Buxgalter") {
                return $this->redirect('/index.php/orders/index');
            } else {
                Yii::$app->user->logout();
                return $this->redirect('index.php/site/login');
            }
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'main-login';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('index');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('index');
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/index.php/site/login');
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
