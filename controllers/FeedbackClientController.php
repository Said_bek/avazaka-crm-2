<?php

namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\Users;
use app\models\Clients;
use app\models\ClientBalls;
use app\models\FeedbackClient;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use function GuzzleHttp\Promise\all;

/**
 * FeedbackClientController implements the CRUD actions for FeedbackClient model.
 */
class FeedbackClientController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => FeedbackClient::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => ClientBalls::find()->where(['=', 'status', 1])->andWhere(['=', 'feedback_client_id', $model->id])->orderBy(['created_date' => SORT_DESC]),
        ]);

        $clients = Clients::find()->all();

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'clients' => $clients,
        ]);
    }

    public function actionCreate()
    {
        $model = new FeedbackClient();
        $model->status = 1;
        if ($model->load(Yii::$app->request->post())) {
            $model->title_uz = base64_encode($model->title_uz);
            $model->title_ru = base64_encode($model->title_ru);
            $model->created_date = date('Y-m-d H:i:s');
            if ($model->save()) {
                //log
                if (true) {
                    $user_id = Yii::$app->user->id;

                    $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                    $userId = $selectUsers->id;

                    eventUser($userId, date('Y-m-d H:i:s'), base64_decode($model->title_uz), "Savol qo'shildi", 'Klient uchun savol');
                }

                // create ball
                if (true) {
                    $clients = Clients::find()->where(['not', ['chat_id' => null]])->all();
                    if (isset($clients) && !empty($clients)) {
                        foreach ($clients as $client) {
                            $client_ball = new ClientBalls;
                            $client_ball->feedback_client_id = $model->id;
                            $client_ball->created_date = date('Y-m-d H:i:s');
                            $client_ball->client_id = $client->id;
                            $client_ball->status = 0;
                            $client_ball->ball = null;
                            $client_ball->description = null;
                            $client_ball->save();
                        }
                    }
                }
            }
            return $this->redirect(['/index.php/feedback-client/view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->title_uz = base64_encode($model->title_uz);
            $model->title_ru = base64_encode($model->title_ru);
            if ($model->save()) {
                //                          START ADD EVENT
                $user_id = Yii::$app->user->id;

                $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                $userId = $selectUsers->id;

                eventUser($userId, date('Y-m-d H:i:s'), base64_decode($model->title_uz), "Savol o'zgartirildi", 'Klient uchun savol');

                //
            }
            return $this->redirect(['/index.php/feedback-client/view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $questionName = $this->findModel($id)->title;
        $this->findModel($id)->delete();

        //                          START ADD EVENT
        $user_id = Yii::$app->user->id;

        $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
        $userId = $selectUsers->id;

        eventUser($userId, date('Y-m-d H:i:s'), base64_decode($questionName), "Savol o'chirildi", 'Klient uchun savol');

        //

        return $this->redirect(['/index.php/feedback-client/index']);
    }

    public function actionBalls()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ClientBalls::find()->where(['=', 'status', 1])->orderBy(['created_date' => SORT_DESC]),
        ]);

        $questions = FeedbackClient::find()->all();
        $clients = Clients::find()->all();

        return $this->render('balls', [
            'dataProvider' => $dataProvider,
            'questions' => $questions,
            'clients' => $clients,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = FeedbackClient::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
