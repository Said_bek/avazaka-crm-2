<?php

namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\OrderMaterials;
use app\models\SectionTimes;
use app\models\RequiredMaterials;
use app\models\SectionOrdersControl;
use app\models\OrderStep;
use app\models\UsersSection;
use app\models\Clients;
use app\models\PausedOrders;
use app\models\Sections;
use app\models\OrderCategories;
use app\models\BranchCategories;
use app\models\Branch;
use app\models\Bot;
use app\models\UserModule;
use app\models\SectionOrders;
use app\models\Category;
use app\models\ClientDebtors;
use app\models\ClientWallet;
use app\models\Details;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Users;
use app\models\Events;
use app\models\OrderDetails;
use app\models\SquareSettings;
use yii\data\Pagination;

function bot($method, $data = [])
{
    $url = 'https://api.telegram.org/bot1594810052:AAHQEku5Q3tslozhq7uGiUpEB6oGUtzUXfs/' . $method;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $res = curl_exec($ch);

    if (curl_error($ch)) {
        var_dump(curl_error($ch));
    } else {
        return json_decode($res);
    }
}

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/index.php/site/login');
        } else {
            $user_module = UserModule::find()->where(['=', 'user_id', Yii::$app->user->identity->getId()])->andWhere(['=', 'module_id', 1])->andWhere(['=', 'status', 1])->one();
            if (!(isset($user_module) && !empty($user_module))) {
                Yii::$app->user->logout();
                return $this->redirect('/index.php/site/login');
                // return $this->redirect('/index.php/user-setting/index');
            }
        }
    }


    public function actionIndex()
    {
        $sections = Sections::find()->orderBy(['order_column' => SORT_ASC])->all();
        $static_sections_control = SectionOrdersControl::find()
            ->where([
                'section_id' => 0,
                'exit_date' => NULL
            ])
            ->orderBy([
                'id' => SORT_DESC
            ])
            ->all();
        return $this->render('index', [
            'sections' => $sections,
            'static_sections_control' => $static_sections_control,
        ]);
    }

    public function actionView()
    {

        $model = Orders::find()
            ->where([
                'status' => 1
            ])
            ->all();

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionCreate()
    {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (isset($_GET['id'])) {
                $id = intval($_GET['id']);
                $order = $this->findModel($id);

                //LOG
                if (true) {
                    $user_id = Yii::$app->user->id;

                    $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                    $userId = $selectUsers->id;

                    $selectOrder = Orders::find()->where(['id' => $id])->one();
                    $orderName = $selectOrder->title;

                    eventUser($userId, date('Y-m-d H:i:s'), $orderName, 'Buyurtma o`chirildi', 'Buyurtmalar');
                }

                //Delete All
                if (true) {
                    // step
                    $order_step = OrderStep::find()
                        ->where([
                            'order_id' => $id
                        ])
                        ->all();

                    if (!empty($order_step)) {
                        foreach ($order_step as $key => $value) {
                            $value->delete();
                        }
                    }


                    // category
                    $order_cat = OrderCategories::find()
                        ->where([
                            'order_id' => $id
                        ])
                        ->all();

                    if (!empty($order_cat)) {
                        foreach ($order_cat as $key => $value) {
                            $value->delete();
                        }
                    }

                    // paused
                    $paused_orders = PausedOrders::find()
                        ->where([
                            'order_id' => $id
                        ])
                        ->all();

                    if (!empty($paused_orders)) {
                        foreach ($paused_orders as $key => $value) {
                            $value->delete();
                        }
                    }

                    // section_orders
                    $section_orders = SectionOrders::find()
                        ->where([
                            'order_id' => $id
                        ])
                        ->all();

                    if (!empty($section_orders)) {
                        foreach ($section_orders as $key => $value) {
                            $value->delete();
                        }
                    }

                    // section_order_control
                    $section_order_control = SectionOrdersControl::find()
                        ->where([
                            'order_id' => $id
                        ])
                        ->all();

                    if (!empty($section_order_control)) {
                        foreach ($section_order_control as $key => $value) {
                            $value->delete();
                        }
                    }

                    // section_order_deteail
                    $section_order_details = OrderDetails::find()
                        ->where([
                            'order_id' => $id
                        ])
                        ->all();

                    if (!empty($section_order_details)) {
                        foreach ($section_order_details as $key => $value) {
                            $value->delete();
                        }
                    }
                }

                //Client wallet
                if (true) {
                    $client_wallet = ClientWallet::find()->where(['=', 'client_id', $order->client_id])->one();
                    $this_order_price = $order->price;
                    $debt_price = 0;
                    $number_price_type = $order->price_type;
                    if ($order->price_type == 1) {
                        $price_type = 'som';
                    } else {
                        $price_type = 'dollar';
                    }

                    $this_order_debtor = ClientDebtors::find()
                        ->where(['=', 'client_id', $order->client_id])
                        ->andWhere(['=', 'order_id', $order->id])
                        ->one();
                    if (true) {
                        if (isset($this_order_debtor) && !empty($this_order_debtor)) {
                            if ($this_order_debtor->debt_status == 1) {
                                $this_order_price += $this_order_debtor->$price_type;
                            } else {
                                $debt_price = $this_order_debtor->$price_type;
                            }
                            $this_order_debtor->delete();
                        }
                    }

                    $diff_debt_and_price = $this_order_price - $debt_price;
                    if ($diff_debt_and_price > 0) {
                        $client_debtors = ClientDebtors::find()
                            ->where(['=', 'client_id', $order->client_id])
                            ->andWhere(['=', 'debt_status', 0])
                            ->andWhere(['!=', $price_type, 0])
                            ->all();
                        if (isset($client_debtors) && !empty($client_debtors)) {
                            //Qarzlar ochrish
                            foreach ($client_debtors as $client_debt) {
                                $delete_debt = $client_debt->$price_type - $diff_debt_and_price;
                                if ($delete_debt > 0) {
                                    $client_debt->debt_status = 1;
                                    $client_debt->$price_type = 0;
                                    $client_debt->save();
                                    $diff_debt_and_price = $delete_debt;
                                } else {
                                    if ($delete_debt == 0) {
                                        $client_debt->debt_status = 1;
                                        $client_debt->$price_type = 0;
                                        $client_debt->save();
                                        $diff_debt_and_price = 0;
                                    } else {
                                        $client_debt->$price_type = abs($delete_debt);
                                        $client_debt->save();
                                        $diff_debt_and_price = 0;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if ($diff_debt_and_price > 0) {
                        $client_wallet->$price_type = $client_wallet->$price_type + $diff_debt_and_price;
                        $client_wallet->wallet_status = 1;
                        $client_wallet->save();
                    }
                }


                if ($order->delete()) {
                    return ['status' => 'success'];
                }
            }
        }
    }


    // open-modal
    public function actionOpenModal()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (isset($_GET['order_id']) && isset($_GET['section_id'])) {
                $order_id = intval($_GET['order_id']);
                $section_id = intval($_GET['section_id']);

                $order = Orders::findOne($order_id);
                $order_control = SectionOrdersControl::find()->where([
                    'order_id' => $order_id,
                    'exit_date' => NULL
                ])->one();

                $order_details = OrderDetails::find()->where(['=', 'order_id', $order_id])->all();

                $details = Details::find()->all();
                $time_arr = [];
                if (isset($order_details) && !empty($order_details)) {
                    foreach ($order_details as $order_detail) {
                        foreach ($details as $detail) {
                            if ($order_detail->detail_id == $detail->id) {
                                $time_arr[$detail->title] = $order_detail->count;
                            }
                        }
                    }
                }
                $order_details = $time_arr;


                $pause = PausedOrders::find()->where([
                    'order_id' => $order_id,
                    'end_date' => '9999-12-31'
                ])->orderBy([
                    'id' => SORT_DESC
                ])->one();

                if (!isset($pause) || empty($pause)) {
                    $pause = false;
                }

                $body_ajax = false;
                if (isset($order_control->id)) {
                    $body_ajax = $order_control->id;
                }

                return [
                    'header' => '<h3>' . $order->title . '</h3>',
                    'status' => 'success',
                    'content' => $this->renderAjax('open-modal.php', [
                        'order' => $order,
                        'pause' => $pause,
                        'order_control' => $order_control,
                        'order_details' => $order_details,
                    ]),
                    'order_control' => $body_ajax,
                ];
            } else {
                return [
                    'status' => 'failure',
                    'content' => 'Ma`lumotlarda xatolik!',
                ];
            }
        }
    }


    // order-end
    public function actionOrderEnd()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (isset($_GET['order_id']) && isset($_GET['section_id'])) {
                $order_id = intval($_GET['order_id']);
                $section_id = intval($_GET['section_id']);

                $order = Orders::findOne($order_id);
                $order->status = 0;
                $orderName = $order->title;
                $order->end_date = date("Y-m-d H:i:s");
                if ($order->save()) {
                    //                          START ADD EVENT

                    $user_id = Yii::$app->user->id;

                    $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                    $userId = $selectUsers->id;

                    eventUser($userId, date('Y-m-d H:i:s'), $orderName, 'Buyurtma bitdi', 'Buyurtmalar');

                    //                          END ADD EVENT

                    $section_orders = SectionOrders::find()
                        ->where([
                            'order_id' => $order_id
                        ])
                        ->all();

                    if (!empty($section_orders)) {
                        foreach ($section_orders as $key => $value) {
                            $value->status = 0;
                            // if ($value->exit_date == NULL) {
                            $value->exit_date = date("Y-m-d H:i:s");
                            $value->step = 1;
                            // }
                            $value->save();
                        }
                    }



                    return ['status' => 'success'];
                }
            } else {
                return [
                    'status' => 'failure',
                    'content' => 'Ma`lumotlarda xatolik!',
                ];
            }
        }
    }

    // add-order-modal
    public function actionAddOrderModal()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $branchs = Branch::find()->where(['=', 'status', 1])->all();
            $clients = Clients::find()->where(['=', 'status', 1])->all();
            $categorys = Category::find()->where(['=', 'status', 1])->all();
            $order = Orders::find()->orderBy(['id' => SORT_DESC])->one();
            $order_code = 1;
            if (isset($order) && !empty($order)) {
                $order_code += $order->id;
            }

            $time_arr = [];
            if (isset($branchs) && !empty($branchs)) {
                foreach ($branchs as $branch) {
                    $time_arr[$branch->id] = $branch->title;
                }
            }
            $branchs = $time_arr;

            $time_arr = [];
            if (isset($clients) && !empty($clients)) {
                foreach ($clients as $client) {
                    $time_arr[$client->id] = base64_decode($client->full_name);
                }
            }
            $clients = $time_arr;
            $time_arr = [];
            if (isset($categorys) && !empty($categorys)) {
                foreach ($categorys as $category) {
                    $time_arr[$category->id] = $category->title;
                }
            }
            $categorys = $time_arr;
            $header = '<div class="row">
                <div class="col-md-6"><h3>Buyurtma qo`shish</h3></div>
                <div class="col-md-6"><h3>Buyurtma raqami ' . $order_code . '</h3></div>
            </div>';
            return [
                'header' => $header,
                'status' => 'success',
                'content' => $this->renderAjax('add-order-modal.php', [
                    'branchs' => $branchs,
                    'clients' => $clients,
                    'categorys' => $categorys,
                ]),
            ];
            // return [
            //     'status' => 'failure',
            //     'content' => 'Ma`lumotlarda xatolik!',
            // ];
        }
    }

    public function actionGetAllSections()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['count']) && (!empty($_GET['count']) || $_GET['count'] >= 0)) {

                $sections = Sections::find()->where(['=', 'status', 1])->all();
                $time_arr = [];
                if (isset($sections) && !empty($sections)) {
                    foreach ($sections as $section) {
                        $time_arr[$section->id] = $section->title;
                    }
                }
                $sections = $time_arr;
                return [
                    'status' => 'success',
                    'content' => $this->renderAjax('get-all-sections.php', [
                        'sections' => $sections,
                        'count' => $_GET['count']
                    ])
                ];
            } else {
                return [
                    'status' => 'fail'
                ];
            }
        }
    }

    public function actionGetAllDetails()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['count']) && (!empty($_GET['count']) || $_GET['count'] >= 0)) {

                $details = Details::find()->where(['=', 'status', 1])->all();
                $time_arr = [];
                if (isset($details) && !empty($details)) {
                    foreach ($details as $section) {
                        $time_arr[$section->id] = $section->title;
                    }
                }
                $details = $time_arr;
                return [
                    'status' => 'success',
                    'content' => $this->renderAjax('get-all-details.php', [
                        'details' => $details,
                        'count' => $_GET['count']
                    ])
                ];
            } else {
                return [
                    'status' => 'fail'
                ];
            }
        }
    }


    // add-section
    public function actionAddSection()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $sections = Sections::find()
                ->where(['is not', 'order_column', NULL])
                ->orderBy(['order_column' => SORT_ASC])
                ->all();
            if ($_GET['i']) {
                $i = intval($_GET['i']);
            }
            $section_id = intval($_GET['section_id']);


            return [
                'status' => 'success',
                'count' => $i,
                'content' => $this->renderAjax('add-section.php', [
                    'sections' => $sections,
                    'i' => $i,
                    'section_id' => $section_id,
                ]),
            ];
        }
    }

    // save-order
    public function actionSaveOrder()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (!empty($_POST)) {
                //Validation
                if (true) {
                    if (empty(preg_replace('/\s+/', '', $_POST['title']))) {
                        return ['status' => 'failure_title'];
                    }

                    if (empty(preg_replace('/\s+/', '', $_POST['branch']))) {
                        return ['status' => 'failure_branch'];
                    }

                    if (empty(preg_replace('/\s+/', '', $_POST['client']))) {
                        return ['status' => 'failure_client'];
                    }

                    if (empty(preg_replace('/\s+/', '', $_POST['category']))) {
                        return ['status' => 'failure_category'];
                    }

                    if (empty(preg_replace('/\s+/', '', $_POST['end_date']))) {
                        return ['status' => 'failure_end_date'];
                    }

                    if (empty(preg_replace('/\s+/', '', $_POST['square']))) {
                        return ['status' => 'failure_square'];
                    }

                    if (empty(preg_replace('/\s+/', '', $_POST['price']))) {
                        return ['status' => 'failure_price'];
                    }

                    if (empty(preg_replace('/\s+/', '', $_POST['price_type']))) {
                        return ['status' => 'failure_price_type'];
                    }

                    if (empty($_POST['sections']) || count($_POST['sections']) <= 0) {
                        return ['status' => 'failure_sections'];
                    }
                    if (empty($_POST['details']) || count($_POST['details']) <= 0) {
                        return ['status' => 'failure_details'];
                    }
                }


                //Before save
                if (true) {
                    $send_notifaction = false;
                    if (isset($_POST['confirm_end_date']) && !empty($_POST['confirm_end_date'])) {
                        if ($_POST['confirm_end_date'] == $_POST['end_date']) {
                            $send_notifaction = false;
                        } else {
                            $send_notifaction = true;
                        }
                    } else {
                        $send_notifaction = true;
                    }

                    if ($send_notifaction) {
                        $square_limit = SquareSettings::find()->where(['<', 'start_date', date('Y-m-d H:i:s')])->andWhere(['end_date' => null])->orderBy(['id' => SORT_DESC])->one();
                        $this_order_end_date = date('Y-m-d', strtotime($_POST['end_date']));

                        if (isset($square_limit) && !empty($square_limit)) {
                            $sql_comannd = "SELECT SUM(ord.square) FROM orders AS ord
                            WHERE ord.pause = 0 AND	ord.dead_line >= '" . $this_order_end_date . " 00:00:00' AND ord.dead_line <= '" . $this_order_end_date . " 23:59:59'";
                            $limit = Yii::$app->db->createCommand($sql_comannd)->queryOne();
                            if (isset($limit) && !empty($limit)) {
                                $limit_sum = $limit['sum'] + $_POST['square'];
                                if ($square_limit->limit <= $limit_sum) {
                                    return [
                                        'status' => 'notification',
                                        'limit' => $square_limit->limit,
                                        'all_limit' => $limit_sum,
                                    ];
                                }
                            }
                        }
                    }
                }


                //Save
                if (true) {
                    //Varible seting
                    if (true) {
                        $title = htmlspecialchars($_POST['title']);
                        $branch = intval($_POST['branch']);
                        $client = intval($_POST['client']);
                        $category = intval($_POST['category']);
                        $square = $_POST['square'];
                        $price = $_POST['price'];
                        $price_type = $_POST['price_type'];
                        $deadline = date("Y-m-d H:i:s", strtotime(date($_POST['end_date'])));
                        $description = htmlspecialchars($_POST['description']);

                        $status = 0;

                        $transaction = Yii::$app->db->beginTransaction();
                    }
                    try {
                        //Order seting
                        if (true) {
                            $order = new Orders();
                            $order->title = $title;
                            $order->branch_id = $branch;
                            $order->user_id = Yii::$app->user->identity->getId();
                            $order->client_id = $client;
                            $order->created_date = date("Y-m-d H:i:s");
                            $order->dead_line = $deadline;
                            $order->status = 1;
                            $order->category_id = $category;
                            $order->pause = 0;
                            $order->description = $description;
                            $order->price_type = $price_type;
                            $order->price = $price;
                            $order->square = $square;
                        }

                        //After order saving
                        if ($order->save()) {
                            //Logging
                            if (true) {
                                $user_id = Yii::$app->user->id;

                                $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                                $userId = $selectUsers->id;

                                eventUser($userId, date('Y-m-d H:i:s'), $title, 'Buyurtma qo`shildi', 'Buyurtmalar');
                            }

                            //Client wallet
                            if (true) {
                                $client_wallet = ClientWallet::find()->where(['=', 'client_id', $order->client_id])->one();
                                if (isset($client_wallet) && !empty($client_wallet)) {
                                    if ($client_wallet->wallet_status == 1) {
                                        //Minusing money
                                        $order_price = $order->price;
                                        if ($order->price_type == 1) {
                                            $wallet_price = $client_wallet->som;
                                            $other_price =  $client_wallet->dollar;
                                        } else {
                                            $wallet_price = $client_wallet->dollar;
                                            $other_price =  $client_wallet->som;
                                        }
                                        if ($wallet_price == 0) {
                                            $client_debtor = new ClientDebtors;
                                            if ($order->price_type == 1) {
                                                $client_debtor->som = $order->price;
                                                $client_debtor->dollar = 0;
                                            } else {
                                                $client_debtor->som = 0;
                                                $client_debtor->dollar = $order->price;
                                            }
                                            $client_debtor->client_id = $order->client_id;
                                            $client_debtor->debt_status = 0;
                                            $client_debtor->order_id = $order->id;
                                            $client_debtor->created_date = date('Y-m-d H:i:s');
                                            $client_debtor->save();

                                            if ($other_price == 0) {
                                                $client_wallet->wallet_status = 0;
                                                $client_wallet->save();
                                            }
                                        } else {
                                            $wallet_cutted = $wallet_price - $order_price;
                                            if ($wallet_cutted > 0) {
                                                if ($order->price_type == 1) {
                                                    $client_wallet->som = $wallet_cutted;
                                                } else {
                                                    $client_wallet->dollar = $wallet_cutted;
                                                }
                                                $client_wallet->save();
                                            } else {
                                                if ($wallet_cutted == 0) {
                                                    if ($order->price_type == 1) {
                                                        $client_wallet->som == 0;
                                                    } else {
                                                        $client_wallet->dollar == 0;
                                                    }
                                                    $client_wallet->save();
                                                } else {
                                                    $wallet_cutted = abs($wallet_cutted);

                                                    $client_debtor = new ClientDebtors;
                                                    $client_debtor->debt_status = 0;
                                                    $client_debtor->order_id = $order->id;
                                                    $client_debtor->created_date = date('Y-m-d H:i:s');
                                                    $client_debtor->client_id = $order->client_id;

                                                    if ($order->price_type == 1) {
                                                        $client_wallet->som = 0;
                                                        $client_debtor->som = $wallet_cutted;
                                                        $client_debtor->dollar = 0;
                                                    } else {
                                                        $client_wallet->dollar = 0;
                                                        $client_debtor->dollar = $wallet_cutted;
                                                        $client_debtor->som = 0;
                                                    }

                                                    if (
                                                        $client_wallet->som == 0 &&
                                                        $client_wallet->dollar == 0
                                                    ) {
                                                        $client_wallet->wallet_status = 0;
                                                    }
                                                    $client_debtor->save();
                                                    $client_wallet->save();
                                                }
                                            }
                                        }
                                    } else {
                                        //Add debtors
                                        $client_debtor = new ClientDebtors;
                                        if ($order->price_type == 1) {
                                            $client_debtor->som = $order->price;
                                            $client_debtor->dollar = 0;
                                        } else {
                                            $client_debtor->som = 0;
                                            $client_debtor->dollar = $order->price;
                                        }
                                        $client_debtor->client_id = $order->client_id;
                                        $client_debtor->debt_status = 0;
                                        $client_debtor->order_id = $order->id;
                                        $client_debtor->created_date = date('Y-m-d H:i:s');
                                        $client_debtor->save();
                                    }
                                } else {
                                    //Create new client wallet and debtors
                                    $new_wallet = new ClientWallet;
                                    $new_wallet->som = 0;
                                    $new_wallet->dollar = 0;
                                    $new_wallet->client_id = $order->client_id;
                                    $new_wallet->wallet_status = 0;
                                    $new_wallet->save();

                                    $new_debtors = new ClientDebtors;

                                    $new_debtors->client_id = $order->client_id;
                                    $new_debtors->debt_status = 0;

                                    if ($order->price_type == 1) {
                                        $new_debtors->som = $order->price;
                                        $new_debtors->dollar = 0;
                                    } else {
                                        $new_debtors->som = 0;
                                        $new_debtors->dollar = $order->price;
                                    }
                                    $new_debtors->order_id = $order->id;
                                    $new_debtors->created_date = date('Y-m-d H:i:s');
                                    $new_debtors->save();
                                }
                            }

                            //save detail
                            if (true) {
                                if (!empty($_POST['details'])) {
                                    foreach ($_POST['details'] as $detail_post) {
                                        $detail_count = preg_replace('~((\.|\,).+$|[^0-9])+~', '', $detail_post['count']);
                                        if (intval($detail_count) == 0) {
                                            $detail_count = '1';
                                        }
                                        $detail_count = intval($detail_count);
                                        $order_detail = OrderDetails::find()
                                            ->where(['=', 'order_id', $order->id])
                                            ->andWhere(['=', 'detail_id', $detail_post['id']])
                                            ->one();
                                        if (isset($order_detail) && !empty($order_detail)) {
                                            $order_detail->count = $order_detail->count + $detail_count;
                                            $order_detail->save();
                                        } else {
                                            $order_detail = new OrderDetails;
                                            $order_detail->order_id = $order->id;
                                            $order_detail->detail_id = $detail_post['id'];
                                            $order_detail->count = $detail_count;
                                            $order_detail->save();
                                        }
                                    }
                                } else {
                                    $status++;
                                }
                            }

                            //save sections
                            if (true) {
                                if (!empty($_POST['sections'])) {
                                    $i = 1;
                                    foreach ($_POST['sections'] as  $post_sections) {
                                        if ($i == 1) {
                                            $section_orders = new SectionOrdersControl();
                                            $section_orders->order_id = $order->id;
                                            $section_orders->section_id = 0;
                                            $section_orders->enter_date = date('Y-m-d H:i:s');
                                            $section_orders->save();
                                        }
                                        $order_step = new OrderStep();
                                        $order_step->order_id = $order->id;
                                        $order_step->section_id = $post_sections;
                                        $order_step->order_column = $i;
                                        $order_step->status = 0;
                                        $order_step->save();

                                        $i++;
                                    }
                                } else {
                                    $status++;
                                }
                            }
                        }

                        if ($status == 0) {
                            $transaction->commit();
                            return ['status' => 'success'];
                        } else {
                            $transaction->rollBack();

                            return ['status' => 'failure'];
                        }
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;

                        return ['status' => 'failure'];
                    }
                }
            }
        }
    }

    // pause
    public function actionPause()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


            if ($_GET['id']) {
                $id = intval($_GET['id']);

                $status = false;
                $model = Orders::findOne($id);
                $orderName = $model->title;
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if (isset($model)) {
                        $model->pause = 1;
                        if ($model->save()) {

                            $user_id = Yii::$app->user->id;

                            $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                            $userId = $selectUsers->id;

                            eventUser($userId, date('Y-m-d H:i:s'), $orderName, 'Buyurtma pauzaga qo`yldi', 'Buyurtmalar');

                            // $find_pause = PausedOrders::find()
                            //     ->where([
                            //         'start_date' => date("Y-m-d"),
                            //         'order_id' => $id
                            //     ])
                            //     ->one();

                            //     if (isset($find_pause)) {
                            //         $find_pause->end_date = date("9999-12-31");
                            //         if($find_pause->save()){
                            //             $status = true;
                            //         }
                            //     }
                            // else{
                            $paused = new PausedOrders();
                            $paused->order_id = $id;
                            $paused->start_date = date("Y-m-d H:i:s");
                            $paused->end_date = date("9999-12-31");
                            if ($paused->save()) {
                                $status = true;
                            }
                            // }
                        }
                    }

                    if ($status) {
                        $transaction->commit();
                        return ['status' => 'success'];
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                    return ['status' => 'failure'];
                }
            }
        }
    }


    // active
    public function actionActive()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if ($_GET['id']) {
                $id = intval($_GET['id']);
                $pause_id = intval($_GET['pause_id']);

                $model = Orders::findOne($id);
                $orderName = $model->title;

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if (isset($model)) {
                        $model->pause = 0;
                        if ($model->save()) {
                            $user_id = Yii::$app->user->id;

                            $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                            $userId = $selectUsers->id;

                            eventUser($userId, date('Y-m-d H:i:s'), $orderName, 'Buyurtma faollashtirildi', 'Buyurtmalar');

                            $paused = PausedOrders::findOne($pause_id);
                            $paused->end_date = date("Y-m-d H:i:s");

                            if ($paused->save()) {
                                $status = true;
                            }
                        }
                    }

                    if ($status) {
                        $transaction->commit();
                        return ['status' => 'success'];
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                    return ['status' => 'failure'];
                }
            }
        }
    }

    public function actionFindTime()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if ($_GET['id']) {
                $id = intval($_GET['id']);

                $model = SectionTimes::find()
                    ->where(['section_id' => $id])
                    ->andWhere([
                        '<=', 'start_date', date('Y-m-d')
                    ])
                    ->andWhere([
                        '>=', 'end_date', date('Y-m-d')
                    ])
                    ->one();

                return [
                    'status' => 'success',
                    'content' => $model->work_time
                ];
            }
        }
    }


    public function actionStartOrder()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if ($_GET['id'] && $_GET['order_id']) {
                $id = intval($_GET['id']);
                $order_id = intval($_GET['order_id']);

                $order = Orders::findOne($order_id);
                $orderName = $order->title;

                $model = SectionOrdersControl::findOne($id);

                if (isset($model) && isset($order)) {
                    $status = 0;
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $model->exit_date = date('Y-m-d H:i:s');
                        if ($model->save()) {
                            //log
                            if (true) {
                                $user_id = Yii::$app->user->id;

                                $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                                $userId = $selectUsers->id;

                                eventUser($userId, date('Y-m-d H:i:s'), $orderName, 'Buyurtma boshlandi', 'Buyurtmalar');
                            }

                            $order_step = OrderStep::find()
                                ->where([
                                    'order_id' => $order_id
                                ])
                                ->orderBy(['order_column' => SORT_ASC])
                                ->one();


                            if (isset($order_step) && !empty($order_step)) {
                                $section_orders = new SectionOrders();
                                $section_orders->order_id = $order_id;
                                $section_orders->section_id = $order_step->section_id;
                                $section_orders->enter_date = date("Y-m-d H:i:s");
                                $order->start_time = date("Y-m-d H:i:s");
                                $section_orders->status = 1;
                                $section_orders->order_step_id = $order_step->id;
                                $section_orders->save();
                                $order->save();
                            }
                        }

                        $transaction->commit();
                        return ['status' => 'success'];
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                        return ['status' => 'failure'];
                    }
                }
            }
        }
    }

    // back_order
    public function actionBackOrder()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if ($_GET['id']) {
                $id = intval($_GET['id']);

                $order = Orders::findOne($id);
                $orderName = $order->title;

                //                          START ADD EVENT

                $user_id = Yii::$app->user->id;

                $selectUsers = Users::find()->where(['user_id' => $user_id])->one();
                $userId = $selectUsers->id;

                eventUser($userId, date('Y-m-d H:i:s'), $orderName, 'Buyurtma ortga qaytarildi', 'Buyurtmalar');

                //                          END ADD EVENT
                $model = SectionOrders::find()
                    ->where([
                        'order_id' => $id,
                        'exit_date' => NULL
                    ])
                    ->one();

                $count = SectionOrders::find()
                    ->where([
                        'order_id' => $id
                    ])->count();

                $status = false;
                if (isset($model)) {
                    if ($count == 1) {
                        $section_orders_control = SectionOrdersControl::find()
                            ->where([
                                'order_id' => $id,
                            ])
                            ->andWhere([
                                'is not', 'exit_date', NULL
                            ])
                            ->one();


                        if (isset($section_orders_control)) {
                            $transaction = Yii::$app->db->beginTransaction();
                            try {
                                $section_orders_control->exit_date = NULL;
                                if ($section_orders_control->save()) {
                                    if ($model->delete()) {
                                        $status = true;
                                    }
                                } else {
                                    pre('1111');
                                }

                                if ($status) {
                                    $transaction->commit();
                                    return ['status' => 'success'];
                                }
                            } catch (\Exception $e) {
                                $transaction->rollBack();
                                throw $e;
                                return ['status' => 'failure_delete_one'];
                            }
                        } else {
                            return ['status' => 'failure_not_order_section_control'];
                        }
                    } else {
                        $old_model = SectionOrders::find()
                            ->where([
                                'order_id' => $id
                            ])
                            ->andWhere([
                                '!=', 'id', $model->id
                            ])
                            ->andWhere([
                                'is not', 'exit_date', NULL
                            ])
                            ->orderBy([
                                'id' => SORT_DESC
                            ])
                            ->one();
                        $transaction = Yii::$app->db->beginTransaction();
                        try {
                            if (isset($old_model)) {
                                $old_model->exit_date = NULL;
                                $old_model->step = NULL;
                                if ($old_model->save()) {
                                    $order_step = OrderStep::find()
                                        ->where([
                                            'order_id' => $id,
                                            'section_id' => $old_model->section_id
                                        ])
                                        ->one();

                                    if (isset($order_step)) {
                                        $order_step->status = 0;
                                        if ($order_step->save()) {
                                            if ($model->delete()) {
                                                $status = true;
                                            }
                                        }
                                    }
                                } else {
                                    pre('3333');
                                }
                            } else {
                                return ['status' => 'failure_back'];
                            }

                            if ($status) {
                                $section_users = UsersSection::find()
                                    ->where([
                                        'section_id' => $order_step->section_id,
                                    ])
                                    ->all();
                                if (!empty($section_users)) {
                                    foreach ($section_users as $key => $value) {

                                        if (isset($value->users) && $value->users->chat_id != NULL) {

                                            $lookOrders = $order->id . "_lookOrders_" . $order_step->section_id;
                                            $lookOrders = json_encode([
                                                'inline_keyboard' => [
                                                    [
                                                        ['callback_data' => $lookOrders, 'text' => "Buyurtmani ko`rish"]

                                                    ],
                                                ]
                                            ]);

                                            bot('sendMessage', [
                                                'chat_id' => $value->users->chat_id,
                                                'parse_mode' => 'html',
                                                'text' => "📌<b><i>" . $order->title . "</i></b> nomli buyurtma sizning bo`limingizga o'tdi",
                                                'reply_markup' => $lookOrders

                                            ]);


                                            // -------------------------- //

                                            // bot('sendMessage', [
                                            //     'chat_id' => $value->users->chat_id,
                                            //     'parse_mode' => 'html',
                                            //     'text' => "📌<b><i>".$order->title."</i></b> nomli buyurtma sizning bo`limingizga qaytarib yuborildi 🔄",

                                            // ]);
                                        }
                                    }
                                }

                                $transaction->commit();
                                return ['status' => 'success'];
                            }
                        } catch (\Exception $e) {
                            $transaction->rollBack();
                            throw $e;
                            return ['status' => 'failure_delete_one'];
                        }
                    }
                } else {
                    return ['status' => 'failure_not_section_order'];
                }
            } else {
                return ['status' => 'failure'];
            }
        }
    }


    public function actionComplete()
    {
        $model = Orders::find()
            ->where([
                'status' => 0
            ])
            ->orderBy([
                'id' => SORT_DESC
            ])
            ->all();


        return $this->render('complete', [
            'model' => $model,
        ]);
    }

    public function actionOrderMaterials()
    {
        $model = Orders::find()->all();


        return $this->render('order-materials', [
            'model' => $model,
        ]);
    }


    public function actionViewMaterials($id)
    {
        if (isset($id)) {
            $id = intval($id);
        }

        $order = Orders::findOne($id);

        $model = OrderMaterials::find()
            ->where([
                'order_id' => $id
            ])
            ->all();


        return $this->render('view-materials', [
            'model' => $model,
            'order' => $order,
        ]);
    }

    public function actionDeleteFile($id)
    {
        if (isset($id)) {
            $id = intval($id);
        }

        $model = OrderMaterials::findOne($id);
        $order_id = $model->order_id;
        if (isset($model)) {
            $model->delete();
        }

        return $this->redirect(['/index.php/orders/view-materials', 'id' => $order_id]);
    }

    public function actionChangeDesc()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (!empty($_GET['id'])) {
                $desc = htmlspecialchars($_GET['desc']);
                $id = intval($_GET['id']);

                $model = Orders::findOne($id);
                if (isset($model)) {
                    $model->description = $desc;
                    if ($model->save()) {
                        return ['status' => 'success'];
                    } else {
                        return ['status' => 'failure_save'];
                    }
                } else {
                    return ['status' => 'failure'];
                }
            }
        }
    }



    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
