<?php

namespace app\models;

use Yii;

class Orders extends \yii\db\ActiveRecord
{
    public $section_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['branch_id', 'user_id', 'client_id', 'status', 'chat_id', 'category_id', 'pause', 'parralel', 'price_type', 'glass_id'], 'default', 'value' => null],
            [['branch_id', 'user_id', 'client_id', 'status', 'chat_id', 'category_id', 'pause', 'parralel', 'price_type', 'glass_id'], 'integer'],
            [['created_date', 'dead_line', 'start_time', 'end_date'], 'safe'],
            [['feedback_user', 'feedback_client', 'price', 'square'], 'number'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'branch_id' => 'Branch ID',
            'user_id' => 'User ID',
            'client_id' => 'Client ID',
            'created_date' => 'Created Date',
            'dead_line' => 'Dead Line',
            'status' => 'Status',
            'feedback_user' => 'Feedback User',
            'feedback_client' => 'Feedback Client',
            'chat_id' => 'Chat ID',
            'category_id' => 'Category ID',
            'pause' => 'Pause',
            'start_time' => 'Start Time',
            'description' => 'Description',
            'end_date' => 'End Date',
            'parralel' => 'Parralel',
            'price_type' => 'Price Type',
            'glass_id' => 'Glass ID',
            'price' => 'Price',
            'square' => 'Square',
        ];
    }

    public function getSalary()
    {
        return $this->hasOne(ClientSalary::classname(), ['order_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(Users::classname(), ['id' => 'user_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Clients::classname(), ['id' => 'client_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::classname(), ['id' => 'category_id']);
    }

    public function getBranch()
    {
        return $this->hasOne(Branch::classname(), ['id' => 'branch_id']);
    }


    public function getOrder_step()
    {
        return $this->hasMany(OrderStep::classname(), ['order_id' => 'id'])->orderBy(['order_column' => SORT_ASC]);
    }



    public static function Section_orders($order_id, $section)
    {
        $model = SectionOrders::find()
            ->where([
                'order_id' => $order_id,
                'section_id' => $section,
            ])
            ->one();

        if (isset($model)) {
            return $model->exit_date;
        } else {
            return '-';
        }
    }



    public static function order_step($order_id, $section)
    {
        $model = OrderStep::find()
            ->where([
                'order_id' => $order_id,
                'section_id' => $section
            ])
            ->one();

        if (isset($model)) {
            return $model->deadline;
        } else {
            return '-';
        }
    }


    public function getPause()
    {
        $model = PausedOrders::find()
            ->where([
                'order_id' => $this->id,
                'end_date' => '9999-12-31 00:00:00'
            ])
            ->orderBy(['id' => SORT_DESC])
            ->one();

        if (isset($model))
            return $model;
        else
            return false;
    }

    public function getRequired()
    {
        $model = RequiredMaterialOrder::find()->where([
            'order_id' => $this->id
        ])
            ->all();

        return $model;
    }

    public function getFiles()
    {
        $model = OrderMaterials::find()->where([
            'order_id' => $this->id
        ])
            ->count();

        return $model;
    }


    public function getPauses()
    {

        $model = PausedOrders::find()
            ->where([
                'order_id' => $this->id
            ])
            ->andWhere(['!=', 'end_date', '9999-12-31 00:00:00'])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        $arr = 0;
        foreach ($model as $key => $value) {
            $t1 = strtotime($value->end_date); //kottasi
            $t2 = strtotime($value->start_date);
            $diff = $t1 - $t2;
            $hours = $diff / (60 * 60);
            $arr += $hours;
        }

        return $arr;
    }
}
