<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category_glass".
 *
 * @property int $id
 * @property int|null $category_id
 * @property int|null $glass_id
 */
class CategoryGlass extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_glass';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'glass_id'], 'default', 'value' => null],
            [['category_id', 'glass_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'glass_id' => 'Glass ID',
        ];
    }

    public function getGlass()
    {
        return $this->hasOne(Glass::classname(), ['id' => 'glass_id']);
    }
}
