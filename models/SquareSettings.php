<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "square_settings".
 *
 * @property int $id
 * @property float|null $limit
 * @property string|null $start_date
 * @property string|null $end_date
 */
class SquareSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'square_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['limit', 'required', 'message' => 'Kvadrat-metr qoshing'],
            [['limit'], 'number'],
            [['start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'limit' => 'Limit',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
        ];
    }
}
