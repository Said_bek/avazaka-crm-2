<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "course_settings".
 *
 * @property int $id
 * @property float|null $price
 * @property string|null $start_date
 * @property string|null $end_date
 */
class CourseSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['price', 'required', 'message' => 'Yangi kurs bolish shart'],
            [['price'], 'number'],
            [['start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
        ];
    }
}
