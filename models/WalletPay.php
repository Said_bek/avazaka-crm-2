<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wallet_pay".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $client_id
 * @property float|null $price
 * @property int|null $price_type
 * @property string|null $created_date
 */
class WalletPay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wallet_pay';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'client_id', 'price_type','price','created_date'], 'required','message' => 'Notogri toldirdingiz'],
            [['user_id', 'client_id', 'price_type'], 'integer'],
            [['price'], 'number'],
            [['created_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'client_id' => 'Client ID',
            'price' => 'Price',
            'price_type' => 'Price Type',
            'created_date' => 'Created Date',
        ];
    }
}
