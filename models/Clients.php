<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string|null $full_name
 * @property string|null $phone_number
 * @property int|null $chat_id
 * @property int|null $status
 * @property int|null $branch_id
 * @property int|null $type
 * @property string|null $inn
 * @property string|null $company
 * @property string|null $brith_date
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'phone_number', 'status', 'branch_id', 'type', 'brith_date'], 'required'],
            [['chat_id', 'status', 'inn', 'company'], 'default', 'value' => null],
            [['chat_id', 'status', 'branch_id', 'type'], 'integer'],
            [['inn', 'company'], 'string'],
            [['brith_date'], 'safe'],
            [['full_name'], 'string', 'max' => 50],
            [['phone_number'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'phone_number' => 'Phone Number',
            'chat_id' => 'Chat ID',
            'status' => 'Status',
            'branch_id' => 'Branch ID',
            'type' => 'Type',
            'inn' => 'Inn',
            'company' => 'Company',
            'brith_date' => 'Brith Date',
        ];
    }

    public function getBranch()
    {
        return $this->hasOne(Branch::classname(), ['id' => 'branch_id']);
    }
}
