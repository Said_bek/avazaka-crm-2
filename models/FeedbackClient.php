<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedback_client".
 *
 * @property int $id
 * @property int|null $status
 * @property string|null $title_uz
 * @property string|null $title_ru
 * @property int|null $ball_type
 * @property string|null $created_date
 */
class FeedbackClient extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback_client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_uz', 'title_ru','ball_type'], 'required'],
            [['status'], 'default', 'value' => null],
            [['status', 'ball_type'], 'integer'],
            [['title_uz', 'title_ru'], 'string'],
            [['created_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'title_uz' => 'Title Uz',
            'title_ru' => 'Title Ru',
            'ball_type' => 'Ball Type',
            'created_date' => 'Created Date',
        ];
    }

    public static function client_ball($client_id, $feedback_client_id)
    {
        $model = ClientBalls::find()->where([
            'client_id' => $client_id,
            'feedback_client_id' => $feedback_client_id
        ])
            ->one();

        if (isset($model)) {
            return $model->ball;
        } else {
            return '';
        }
    }
}
