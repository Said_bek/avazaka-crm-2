<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_module".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $module_id
 * @property int|null $status
 */
class UserModule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_module';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'module_id', 'status'], 'default', 'value' => null],
            [['user_id', 'module_id', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'module_id' => 'Module ID',
            'status' => 'Status',
        ];
    }
}
