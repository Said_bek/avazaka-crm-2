<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "role_base_module".
 *
 * @property int $id
 * @property string|null $role
 * @property int|null $module_id
 */
class RoleBaseModule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'role_base_module';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role'], 'string'],
            [['module_id'], 'default', 'value' => null],
            [['module_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role' => 'Role',
            'module_id' => 'Module ID',
        ];
    }
}
