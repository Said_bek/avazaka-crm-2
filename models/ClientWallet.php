<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_wallet".
 *
 * @property int $id
 * @property float|null $som
 * @property float|null $dollar
 * @property int|null $client_id
 * @property int|null $wallet_status
 */
class ClientWallet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_wallet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['som', 'dollar'], 'number'],
            [['client_id', 'wallet_status'], 'default', 'value' => null],
            [['client_id', 'wallet_status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'som' => 'Som',
            'dollar' => 'Dollar',
            'client_id' => 'Client ID',
            'wallet_status' => 'Wallet Status',
        ];
    }
}
