<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $description
 * @property float|null $price
 * @property string|null $created_date
 * @property int|null $module_id
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'module_id'], 'default', 'value' => null],
            [['user_id', 'module_id'], 'integer'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['created_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'description' => 'Description',
            'price' => 'Price',
            'created_date' => 'Created Date',
            'module_id' => 'Module ID',
        ];
    }
}
