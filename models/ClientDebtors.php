<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_debtors".
 *
 * @property int $id
 * @property float|null $som
 * @property float|null $dollar
 * @property int|null $client_id
 * @property int|null $debt_status
 * @property int|null $order_id
 * @property string|null $created_date
 */
class ClientDebtors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_debtors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['som', 'dollar'], 'number'],
            [['client_id', 'debt_status', 'order_id'], 'default', 'value' => null],
            [['client_id', 'debt_status', 'order_id'], 'integer'],
            [['created_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'som' => 'Som',
            'dollar' => 'Dollar',
            'client_id' => 'Client ID',
            'debt_status' => 'Debt Status',
            'order_id' => 'Order ID',
            'created_date' => 'Created Date',
        ];
    }
}
