<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_balls".
 *
 * @property int $id
 * @property int|null $feedback_client_id
 * @property float|null $ball
 * @property string|null $created_date
 * @property int|null $client_id
 * @property string|null $description
 * @property int|null $status
 */
class ClientBalls extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_balls';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['feedback_client_id', 'client_id', 'status'], 'default', 'value' => null],
            [['feedback_client_id', 'client_id', 'status'], 'integer'],
            [['ball'], 'number'],
            [['created_date'], 'safe'],
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'feedback_client_id' => 'Feedback Client ID',
            'ball' => 'Ball',
            'created_date' => 'Created Date',
            'client_id' => 'Client ID',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }
}
