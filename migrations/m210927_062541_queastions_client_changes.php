<?php

use yii\db\Migration;

/**
 * Class m210927_062541_queastions_client_changes
 */
class m210927_062541_queastions_client_changes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //client_balls
        $this->dropColumn('client_balls', 'chat_id');
        $this->dropColumn('client_balls', 'order_id');

        $this->addColumn('client_balls', 'client_id', $this->integer());
        $this->addColumn('client_balls', 'description', $this->text());
        $this->addColumn('client_balls', 'status', $this->smallInteger());



        //feedback_client
        $this->dropColumn('feedback_client', 'title');
        $this->dropColumn('feedback_client', 'type');

        $this->addColumn('feedback_client', 'title_uz', $this->text());
        $this->addColumn('feedback_client', 'title_ru', $this->text());
        $this->addColumn('feedback_client', 'created_date', $this->smallInteger());
        $this->addColumn('feedback_client', 'ball_type', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210927_062541_queastions_client_changes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210927_062541_queastions_client_changes cannot be reverted.\n";

        return false;
    }
    */
}
