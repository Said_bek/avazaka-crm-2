<?php

use yii\db\Migration;

/**
 * Class m210913_015812_add_brith_date_in_clients
 */
class m210913_015812_add_brith_date_in_clients extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients', 'brith_date', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210913_015812_add_brith_date_in_clients cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210913_015812_add_brith_date_in_clients cannot be reverted.\n";

        return false;
    }
    */
}
