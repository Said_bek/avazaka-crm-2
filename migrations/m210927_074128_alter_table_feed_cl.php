<?php

use yii\db\Migration;

/**
 * Class m210927_074128_alter_table_feed_cl
 */
class m210927_074128_alter_table_feed_cl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('feedback_client', 'created_date');
        $this->addColumn('feedback_client', 'created_date', $this->timestamp());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210927_074128_alter_table_feed_cl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210927_074128_alter_table_feed_cl cannot be reverted.\n";

        return false;
    }
    */
}
