<?php

use yii\db\Migration;

/**
 * Class m210914_045542_order_details_and_count_delete
 */
class m210914_045542_order_details_and_count_delete extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('orders', 'count');

        $this->createTable('details', [
            'id' => $this->primaryKey(),
            'title' => $this->text(),
            'status' => $this->integer(),
            'created_date' => $this->timestamp(),
        ]);

        $this->createTable('order_details',[
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'detail_id' => $this->integer(),
            'count' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210914_045542_order_details_and_count_delete cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210914_045542_order_details_and_count_delete cannot be reverted.\n";

        return false;
    }
    */
}
