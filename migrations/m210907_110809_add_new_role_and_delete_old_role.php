<?php

use yii\db\Migration;

/**
 * Class m210907_110809_add_new_role_and_delete_old_role
 */
class m210907_110809_add_new_role_and_delete_old_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "DELETE FROM auth_item WHERE description = 'Permission';";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Admin', 1, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Seh boshlig’i', 1, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Sotuv menejeri', 1, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Brigadir', 1, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Menejer', 1, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Buxgalter ', 1, 'Permission');";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210907_110809_add_new_role_and_delete_old_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210907_110809_add_new_role_and_delete_old_role cannot be reverted.\n";

        return false;
    }
    */
}
