<?php

use yii\db\Migration;

/**
 * Class m210929_064457_abdugafor_messages
 */
class m210929_064457_abdugafor_messages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('messages', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer()->notNull(),
            'message_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210929_064457_abdugafor_messages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210929_064457_abdugafor_messages cannot be reverted.\n";

        return false;
    }
    */
}
