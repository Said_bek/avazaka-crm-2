<?php

use yii\db\Migration;

/**
 * Class m210906_104816_delete_video_and_team_tabels
 */
class m210906_104816_delete_video_and_team_tabels extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('video');
        $this->dropTable('team');
        $this->dropTable('team_schedule');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210906_104816_delete_video_and_team_tabels cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210906_104816_delete_video_and_team_tabels cannot be reverted.\n";

        return false;
    }
    */
}
