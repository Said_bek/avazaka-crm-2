<?php

use yii\db\Migration;

/**
 * Class m210919_193311_category_glass
 */
class m210919_193311_category_glass extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category_glass', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'glass_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210919_193311_category_glass cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210919_193311_category_glass cannot be reverted.\n";

        return false;
    }
    */
}
