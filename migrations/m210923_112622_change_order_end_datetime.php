<?php

use yii\db\Migration;

/**
 * Class m210923_112622_change_order_end_datetime
 */
class m210923_112622_change_order_end_datetime extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('orders', 'end_date', 'timestamp');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210923_112622_change_order_end_datetime cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210923_112622_change_order_end_datetime cannot be reverted.\n";

        return false;
    }
    */
}
