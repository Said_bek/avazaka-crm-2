<?php

use yii\db\Migration;

/**
 * Class m210914_112612_add_insectin_orders_coumn_order_step
 */
class m210914_112612_add_insectin_orders_coumn_order_step extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('section_orders', 'order_step_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210914_112612_add_insectin_orders_coumn_order_step cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210914_112612_add_insectin_orders_coumn_order_step cannot be reverted.\n";

        return false;
    }
    */
}
