<?php

use yii\db\Migration;

/**
 * Class m210913_063204_add_modules
 */
class m210913_063204_add_modules extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = 'INSERT INTO "modules" VALUES (1, \'Buyurtmalar\', 1);';
        $this->execute($sql);
        
        $sql = 'INSERT INTO "modules" VALUES (2, \'Tayyor mahsulotlar ombori\', 2);';
        $this->execute($sql);
        
        $sql = 'INSERT INTO "modules" VALUES (3, \'Foydalanuvchilar\', 3);';
        $this->execute($sql);
        
        $sql = 'INSERT INTO "modules" VALUES (4, \'Seh bo’limlari\', 4);';
        $this->execute($sql);
        
        $sql = 'INSERT INTO "modules" VALUES (5, \'Mijozlar\', 5);';
        $this->execute($sql);
        
        $sql = 'INSERT INTO "modules" VALUES (6, \'Hisobotlar\', 6);';
        $this->execute($sql);
        
        $sql = 'INSERT INTO "modules" VALUES (7, \'Moliya bo’limi\', 7);';
        $this->execute($sql);
        
        $sql = 'INSERT INTO "modules" VALUES (8, \'Sozlamalar\', 8);';
        $this->execute($sql);
        
        $sql = 'INSERT INTO "modules" VALUES (9, \'So’rovnomalar yuborish\', 9);';
        $this->execute($sql);
        
        $sql = 'INSERT INTO "modules" VALUES (10, \'Xizmat turlari\', 10);';
        $this->execute($sql);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210913_063204_add_modules cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210913_063204_add_modules cannot be reverted.\n";

        return false;
    }
    */
}
