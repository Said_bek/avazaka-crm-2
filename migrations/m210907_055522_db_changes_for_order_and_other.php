<?php

use yii\db\Migration;

/**
 * Class m210907_055522_db_changes_for_order_and_other
 */
class m210907_055522_db_changes_for_order_and_other extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        //New table
        $this->createTable('square_settings', [
            'id' => $this->primaryKey(),
            'limit' => $this->double(),
            'start_date' => $this->timestamp(),
            'end_date' => $this->timestamp(),
        ]);

        $this->createTable('course_settings', [
            'id' => $this->primaryKey(),
            'price' => $this->double(),
            'start_date' => $this->timestamp(),
            'end_date' => $this->timestamp(),
        ]);

        $this->createTable('log', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'description' => $this->text(),
            'price' => $this->double(),
            'created_date' => $this->timestamp(),
            'module_id' => $this->integer(),
        ]);

        $this->createTable('user_module', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'module_id' => $this->integer(),
            'status' => $this->integer(),
        ]);

        $this->createTable('modules', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'module_code' => $this->integer(),
        ]);

        $this->createTable('role_base_module', [
            'id' => $this->primaryKey(),
            'role' => $this->text(),
            'module_id' => $this->integer(),
        ]);

        $this->createTable('client_wallet', [
            'id' => $this->primaryKey(),
            'som' => $this->double(),
            'dollar' => $this->double(),
            'client_id' => $this->integer(),
            'wallet_status' => $this->integer(),
        ]);

        $this->createTable('client_debtors', [
            'id' => $this->primaryKey(),
            'som' => $this->double(),
            'dollar' => $this->double(),
            'client_id' => $this->integer(),
            'debt_status' => $this->integer(),
            'order_id' => $this->integer(),
            'created_date' => $this->timestamp(),
        ]);

        $this->createTable('glass', [
            'id' => $this->primaryKey(),
            'title' => $this->text(),
        ]);

        $this->createTable('wallet_pay', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'client_id' => $this->integer(),
            'price' => $this->double(),
            'price_type' => $this->integer(),
            'created_date' => $this->timestamp(),
        ]);

        //Change Table
            //Orders
            $this->dropColumn('orders', 'team_id');
            $this->addColumn('orders', 'price_type', $this->integer());
            $this->addColumn('orders', 'glass_id', $this->integer());
            $this->addColumn('orders', 'count', $this->integer());
            $this->addColumn('orders', 'price', $this->double());
            $this->addColumn('orders', 'square', $this->double());
            
            //Client
            $this->addColumn('clients', 'type', $this->integer());
            $this->addColumn('clients', 'inn', $this->text());
            $this->addColumn('clients', 'company', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210907_055522_db_changes_for_order_and_other cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210907_055522_db_changes_for_order_and_other cannot be reverted.\n";

        return false;
    }
    */
}
