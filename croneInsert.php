<?php
	ini_set('display_errors', true);
	// bunga tema Abdulaziz ---
    date_default_timezone_set('Asia/Tashkent');
    // --- 

	$conn = pg_connect("host=localhost dbname=orginal_db user=postgres password=postgres");
  

    $sql = "SELECT 
		os.deadline,ord.title as order_name ,u.username,u.chat_id,so.id,s.title
	FROM orders AS ord 
	INNER JOIN section_orders AS so ON ord.id = so.order_id
	INNER JOIN order_step AS os ON ord.id = os.order_id AND os.section_id = so.section_id
	INNER JOIN sections AS s ON so.section_id = s.id
	INNER JOIN users_section AS us ON s.id = us.section_id
	INNER JOIN users AS u ON us.user_id = u.id AND u.chat_id IS NOT NULL
	WHERE so.exit_date IS NULL ";
	// echo $sql;
	$result = pg_query($conn,$sql);
	if (pg_num_rows($result) > 0) {
		$sqlTxt = "INSERT INTO otdel_log_message (chat_id,text,status) VALUES ";
		while ($row = pg_fetch_assoc($result)) {
			$time = $row["deadline"];
			$sectoion_title = $row["title"];
			$chat_id_insert = $row["chat_id"];
			// echo "Its time: " .$time;
			$date = date("Y-m-d",strtotime(date($time)));			
			$yestrday = date("Y-m-d",(strtotime("-1 day")));
			$today = date("Y-m-d",(strtotime("now")));
			$nextDay = date("Y-m-d",(strtotime("+1 day")));
			if (strtotime($date) == strtotime($nextDay)) {
				$title = $row["order_name"]." nomli buyurtma ertaga ".$sectoion_title." bo'limidan chiqib ketishiga 1 kun qoldi!";
				// $sqlInsert = "INSERT INTO otdel_log_message (chat_id,text,status) VALUES (".$chat_id_insert.",'".$title."',0)";
				// $resultInsert = pg_query($conn,$sqlInsert);
			} else if (strtotime($date) == strtotime($today)) {
				$title = $row["order_name"]." nomli buyurtma bugun ".$sectoion_title." bo'limidan chiqib ketishi kerak";
				// $sqlInsert = "INSERT INTO otdel_log_message (chat_id,text,status) VALUES (".$chat_id_insert.",'".$title."',0)";
				// $resultInsert = pg_query($conn,$sqlInsert);
			} else if (strtotime($date) == strtotime($yestrday)) {
				$title = $row["order_name"]." nomli buyurtma ".$sectoion_title." bo'limidan 1 kun kechga qolyaptdi!";
				// $sqlInsert = "INSERT INTO otdel_log_message (chat_id,text,status) VALUES (".$chat_id_insert.",'".$title."',0)";
				// $resultInsert = pg_query($conn,$sqlInsert);
			}  else if (strtotime($date) < strtotime($yestrday)) {
				$title = $row["order_name"]." nomli buyurtma ".$sectoion_title." bo`limidan ".$date." kuni  chiqib ketishi kerak edi!";
				// $sqlInsert = "INSERT INTO otdel_log_message (chat_id,text,status) VALUES (".$chat_id_insert.",'".$title."',0)";
				// $resultInsert = pg_query($conn,$sqlInsert);
			}
			$title = str_replace("'", "`", $title);
			$sqlTxt = $sqlTxt.PHP_EOL."(".$chat_id_insert.",'".$title."',0),";
		}
		$str = substr($sqlTxt,0,-1);

		// echo "<pre>";
		// print_r($str);
		// die();
		$resultInsert = pg_query($conn,$str);
	}


		
?>